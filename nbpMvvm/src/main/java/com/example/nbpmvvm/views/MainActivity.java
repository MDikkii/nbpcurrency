package com.example.nbpmvvm.views;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.databinding.Observable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BaseTransientBottomBar;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.example.nbpmvvm.R;
import com.example.nbpmvvm.databinding.ActivityMainBinding;
import com.example.nbpmvvm.models.DayTable;
import com.example.nbpmvvm.models.services.CurrencyRatesIntentService;
import com.example.nbpmvvm.models.services.CurrencyRatesResultReceiver;
import com.example.nbpmvvm.models.syncAdapter.AlarmReceicer;
import com.example.nbpmvvm.viewmodels.MainViewModel;
import com.example.nbpmvvm.viewmodels.MainViewModelImpl;
import com.example.nbpmvvm.views.adapters.DailyCurrencyAdapter;

import java.lang.ref.WeakReference;
import java.util.Calendar;
import java.util.List;

import static com.example.nbpmvvm.models.services.CurrencyRatesIntentService.HISTORIC_ACTION;
import static com.example.nbpmvvm.models.services.CurrencyRatesIntentService.LAST_AVAILABLE_ACTION;
import static com.example.nbpmvvm.models.services.CurrencyRatesIntentService.TODAY_ACTION;
import static com.example.nbpmvvm.models.services.CurrencyRatesResultReceiver.RESULT_RECIVER;


public class MainActivity extends AppCompatActivity implements MainView {
    public static final String CODE = "code";

    MainViewModel mainViewModel;
    Snackbar snackbar;
    private RecyclerView oneDayCurrencyRecyclerView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private boolean isRefreshPossible;
    private boolean isSnackbarDismissed;
    private boolean wasOnCreateCalled;
    private boolean isSnackbarShown;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final ActivityMainBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        mainViewModel = ViewModelProviders.of(this).get(MainViewModelImpl.class);
        binding.setViewModel(mainViewModel);


        Toolbar myToolbar = findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);

        oneDayCurrencyRecyclerView = findViewById(R.id.oneDayCurrencyRecyclerView);
        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);

        setUpSwipeRefreshLayout();
        setUpCurrencyTable();

        mainViewModel.getDate().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                binding.setDate(s);
            }
        });

        mainViewModel.getIsProgressShown().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {
                binding.setIsProgressShown(aBoolean);
            }
        });

        mainViewModel.getIsRefreshPossible().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {
                if (aBoolean) {
                    showOldDataStatus();
                } else {
                    hideOldDataStatus();
                }
                isRefreshPossible = aBoolean;
                binding.setIsRefreshPossible(aBoolean);
            }
        });

        mainViewModel.getServiceAction().addOnPropertyChangedCallback(new Observable.OnPropertyChangedCallback() {
            @Override
            public void onPropertyChanged(Observable sender, int propertyId) {
                startCurrencyRatesIntentService(mainViewModel.getServiceAction().get());
            }
        });

        mainViewModel.getShowToast().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {
                showOldDataToast();
            }
        });

        mainViewModel.getSetUpSyncAlarm().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {
                setUpAlarmForSync();
            }
        });

        mainViewModel.loadData();
        wasOnCreateCalled = true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        isSnackbarDismissed = false;
        if (!wasOnCreateCalled) {
            mainViewModel.checkTodayDataAvailability();
        }
        wasOnCreateCalled = false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_activity_menu, menu);

        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        MenuItem item = menu.findItem(R.id.refresh);

        if (isRefreshPossible) {
            item.setEnabled(true);
            item.setIcon(R.drawable.ic_refresh);
            item.getIcon().setAlpha(255);
        } else {
            item.setEnabled(false);
            item.setIcon(R.drawable.ic_current_data);
            item.getIcon().setAlpha(120);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.refresh:
                mainViewModel.loadTodayData();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setUpSnackbar() {
        snackbar = Snackbar
                .make(findViewById(R.id.mainLayout), R.string.data_unavailable, Snackbar.LENGTH_INDEFINITE);

        snackbar.setAction("Refresh", new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isSnackbarDismissed = true;
                snackbar.dismiss();
                mainViewModel.loadTodayData();
            }
        });

        snackbar.addCallback(getSnackbarCallback());
    }

    @NonNull
    private BaseTransientBottomBar.BaseCallback<Snackbar> getSnackbarCallback() {
        return new BaseTransientBottomBar.BaseCallback<Snackbar>() {
            @Override
            public void onDismissed(Snackbar transientBottomBar, int event) {
                super.onDismissed(transientBottomBar, event);
                isSnackbarDismissed = true;
                isSnackbarShown = false;
            }

            @Override
            public void onShown(Snackbar transientBottomBar) {
                super.onShown(transientBottomBar);
                isSnackbarShown = true;
                isSnackbarDismissed = false;
            }
        };
    }

    private void setUpCurrencyTable() {
        oneDayCurrencyRecyclerView.setAdapter(new DailyCurrencyAdapter(this, mainViewModel));
        oneDayCurrencyRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        oneDayCurrencyRecyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
    }

    private void setUpSwipeRefreshLayout() {
        swipeRefreshLayout.setColorSchemeResources(
                R.color.secondaryColor,
                R.color.secondaryLightColor,
                R.color.secondaryDarkColor);
    }

    private void startCurrencyRatesIntentService(String action) {
        CurrencyRatesResultReceiver currencyRatesResultReceiver = new CurrencyRatesResultReceiver(new Handler(this.getMainLooper()));

        if (action == TODAY_ACTION) {
            currencyRatesResultReceiver.setReceiver(new TodayTableResultReceiverCallback(mainViewModel));
        } else if (action == LAST_AVAILABLE_ACTION) {
            currencyRatesResultReceiver.setReceiver(new LastAvailableTableResultReceiverCallback(mainViewModel));
        } else if (action == HISTORIC_ACTION) {
            currencyRatesResultReceiver.setReceiver(new HistoricResultReceiverCallBack(mainViewModel));
        }

        Intent intent = new Intent(this, CurrencyRatesIntentService.class);
        intent.setAction(action);
        intent.putExtra(RESULT_RECIVER, currencyRatesResultReceiver);
        startService(intent);
    }

    private void showOldDataStatus() {
        if (!isSnackbarDismissed && !isSnackbarShown) {
            setUpSnackbar();
            snackbar.show();
        }
        invalidateOptionsMenu();
    }

    private void hideOldDataStatus() {
        if (isSnackbarShown) {
            snackbar.dismiss();
        }
        invalidateOptionsMenu();
    }

    private void showOldDataToast() {
        if (isSnackbarDismissed) {
            Toast.makeText(this, R.string.no_data, Toast.LENGTH_SHORT).show();
        }
    }

    private void setUpAlarmForSync() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.set(Calendar.HOUR_OF_DAY, 12);
        calendar.set(Calendar.MINUTE, 0);

        AlarmManager alarmMenager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(this, AlarmReceicer.class);
        intent.setAction("com.example.demkowmik.nbpcurrencyviewer.alarms");
        PendingIntent alarmIntent = PendingIntent.getBroadcast(this, 0, intent, 0);
        alarmMenager.set(AlarmManager.RTC, calendar.getTimeInMillis(), alarmIntent);
    }

    private static class TodayTableResultReceiverCallback implements CurrencyRatesResultReceiver.ResultReceiverCallBack<DayTable> {
        private WeakReference<MainViewModel> presenterWeakReference;

        TodayTableResultReceiverCallback(MainViewModel presenter) {
            presenterWeakReference = new WeakReference<>(presenter);
        }

        @Override
        public void onDataReceive(DayTable data) {
            Log.i("SingleDayReceiver", "onDataReceive");
            presenterWeakReference.get().todayTableReceived(data);
        }
    }

    private static class LastAvailableTableResultReceiverCallback implements CurrencyRatesResultReceiver.ResultReceiverCallBack<DayTable> {
        private WeakReference<MainViewModel> presenterWeakReference;

        LastAvailableTableResultReceiverCallback(MainViewModel presenter) {
            presenterWeakReference = new WeakReference<>(presenter);
        }

        @Override
        public void onDataReceive(DayTable data) {
            Log.i("SingleDayReceiver", "onDataReceive");
            presenterWeakReference.get().lastAvailableDayTableReceived(data);
        }
    }

    private class HistoricResultReceiverCallBack implements CurrencyRatesResultReceiver.ResultReceiverCallBack<List<DayTable>> {
        private WeakReference<MainViewModel> presenterWeakReference;

        HistoricResultReceiverCallBack(MainViewModel presenter) {
            presenterWeakReference = new WeakReference<>(presenter);
        }

        @Override
        public void onDataReceive(List<DayTable> data) {
            Log.i("HistoricReceiver", "onDataReceive");
            presenterWeakReference.get().historicTablesReceived();
        }
    }
}
package com.example.nbpmvvm.views.adapters;

import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.Observer;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.nbpmvvm.R;
import com.example.nbpmvvm.databinding.CurrencyRowBinding;
import com.example.nbpmvvm.models.CurrencyRate;
import com.example.nbpmvvm.models.CurrencyRow;
import com.example.nbpmvvm.models.DayTable;
import com.example.nbpmvvm.viewmodels.CurrencyRowViewModel;
import com.example.nbpmvvm.viewmodels.CurrencyRowViewModelImpl;
import com.example.nbpmvvm.viewmodels.MainViewModel;
import com.example.nbpmvvm.views.CurrencyHistoryActivity;

import java.util.ArrayList;
import java.util.List;

import static com.example.nbpmvvm.views.MainActivity.CODE;

/**
 * Created by Mikolaj D. on 04.10.2017.
 */

public class DailyCurrencyAdapter extends RecyclerView.Adapter<CurrencyRowViewHolder> {
    List<CurrencyRowViewModel> rowViewModels;

    public DailyCurrencyAdapter(LifecycleOwner lifecycleOwner, MainViewModel viewModel) {
        rowViewModels = new ArrayList<>();
        viewModel.getTable().observe(lifecycleOwner, new Observer<DayTable>() {
            @Override
            public void onChanged(@Nullable DayTable dayTable) {
                getRowViewModels(dayTable);
            }
        });
    }

    private void getRowViewModels(@Nullable DayTable dayTable) {
        List<CurrencyRate> dataList = (dayTable != null && dayTable.getRates() != null) ? dayTable.getRates() : new ArrayList<CurrencyRate>();
        rowViewModels = new ArrayList<>();
        for (CurrencyRate rate : dataList) {
            rowViewModels.add(new CurrencyRowViewModelImpl(new CurrencyRow(rate.getCode(), String.valueOf(rate.getValue()))));
        }
        notifyDataSetChanged();
    }

    @Override
    public CurrencyRowViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        CurrencyRowBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.currency_row, parent, false);
        CurrencyRowViewHolder holder = new CurrencyRowViewHolder(binding.getRoot(), binding, new CurrencyRowViewHolder.CurrencyRowOnClickListener() {
            @Override
            public void onClick(View view) {
                prepareIntent(view);
            }
        });
        return holder;
    }

    private void prepareIntent(View view) {
        Intent intent = new Intent(view.getContext(), CurrencyHistoryActivity.class);
        TextView tw = view.findViewById(R.id.currencyTitleTextView);
        intent.putExtra(CODE, tw.getText());
        view.getContext().startActivity(intent);
    }

    @Override
    public void onBindViewHolder(CurrencyRowViewHolder holder, int position) {
        holder.getBinding().setViewModel(rowViewModels.get(position));
    }

    @Override
    public int getItemCount() {
        return rowViewModels.size();
    }
}
package com.example.nbpmvvm.views.adapters;

import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.Observer;
import android.databinding.DataBindingUtil;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.nbpmvvm.R;
import com.example.nbpmvvm.databinding.CurrencyRowBinding;
import com.example.nbpmvvm.models.CurrencyRow;
import com.example.nbpmvvm.viewmodels.CurrencyHistoryViewModel;
import com.example.nbpmvvm.viewmodels.CurrencyRowViewModel;
import com.example.nbpmvvm.viewmodels.CurrencyRowViewModelImpl;

import org.joda.time.LocalDate;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Mikolaj D. on 04.10.2017.
 */

public class HistoricCurrencyAdapter extends RecyclerView.Adapter<CurrencyRowViewHolder> {

    List<CurrencyRowViewModel> rowViewModels;

    public HistoricCurrencyAdapter(LifecycleOwner lifecycleOwner, CurrencyHistoryViewModel viewModel) {
        viewModel.getHistoricData().observe(lifecycleOwner, new Observer<Map<LocalDate, Double>>() {
            @Override
            public void onChanged(@Nullable Map<LocalDate, Double> historicData) {
                getRowViewModels(historicData);
            }
        });
    }

    private void getRowViewModels(Map<LocalDate, Double> historicData) {
        rowViewModels = new ArrayList<>();
        for (Map.Entry<LocalDate, Double> d : historicData.entrySet()) {
            rowViewModels.add(new CurrencyRowViewModelImpl(new CurrencyRow(d.getKey().toString("dd-MM-yyyy"), String.valueOf(d.getValue()))));
        }
    }

    @Override
    public CurrencyRowViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        CurrencyRowBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.currency_row, parent, false);
        CurrencyRowViewHolder holder = new CurrencyRowViewHolder(binding.getRoot(), binding, new CurrencyRowViewHolder.CurrencyRowOnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        return holder;
    }

    @Override
    public void onBindViewHolder(CurrencyRowViewHolder holder, int position) {
        holder.getBinding().setViewModel(rowViewModels.get(position));
    }

    @Override
    public int getItemCount() {
        return rowViewModels.size();
    }
}
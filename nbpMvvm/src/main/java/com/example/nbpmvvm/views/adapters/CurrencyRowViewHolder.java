package com.example.nbpmvvm.views.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.example.nbpmvvm.R;
import com.example.nbpmvvm.databinding.CurrencyRowBinding;


/**
 * Created by Mikolaj D. on 11.10.2017.
 */
public class CurrencyRowViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    View currencyRow;
    CurrencyRowOnClickListener listener;
    private CurrencyRowBinding binding;

    public CurrencyRowViewHolder(View itemView, CurrencyRowBinding binding, CurrencyRowOnClickListener listener) {
        super(itemView);

        this.binding = binding;
        this.listener = listener;

        currencyRow = itemView.findViewById(R.id.currencyRow);
        currencyRow.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        listener.onClick(view);
    }

    public CurrencyRowBinding getBinding() {
        return binding;
    }

    public interface CurrencyRowOnClickListener {
        void onClick(View view);
    }
}

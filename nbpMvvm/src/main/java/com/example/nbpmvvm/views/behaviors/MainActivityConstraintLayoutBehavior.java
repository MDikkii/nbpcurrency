package com.example.nbpmvvm.views.behaviors;

import android.animation.ValueAnimator;
import android.content.Context;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by Mikolaj D. on 12.10.2017.
 */

public class MainActivityConstraintLayoutBehavior extends CoordinatorLayout.Behavior<View> {

    public static final int SNACKBAR_ANIMATION_DURATION = 200;
    //onDependentViewChanged is triggered many times, it should start animation
    //only first time for single direction
    private boolean isSnackbarShown = false;
    private int childHeightBeforeSnackbar;
    private float oldTranslation;

    public MainActivityConstraintLayoutBehavior() {
    }

    public MainActivityConstraintLayoutBehavior(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean layoutDependsOn(CoordinatorLayout parent, View child, View dependency) {
        return dependency instanceof Snackbar.SnackbarLayout;
    }

    @Override
    public boolean onDependentViewChanged(CoordinatorLayout parent, final View child, View dependency) {
        float snackbarTranslation = dependency.getTranslationY();
        float translationDifference = oldTranslation - snackbarTranslation;

        if (translationDifference > 0) {
            if (!isSnackbarShown) {
                isSnackbarShown = true;

                childHeightBeforeSnackbar = child.getHeight();
                int snackbarHeight = dependency.getHeight();
                int newHeight = childHeightBeforeSnackbar - snackbarHeight;
                animateChildView(child, childHeightBeforeSnackbar, newHeight);
            }
        } else if (translationDifference < 0) {
            if (isSnackbarShown) {
                isSnackbarShown = false;

                int currentHeight = child.getHeight();
                animateChildView(child, currentHeight, childHeightBeforeSnackbar);
            }
        }
        oldTranslation = snackbarTranslation;
        return true;
    }

    @Override
    public void onDependentViewRemoved(CoordinatorLayout parent, View child, View dependency) {
        if (isSnackbarShown) {
            isSnackbarShown = false;

            int currentHeight = child.getHeight();
            animateChildView(child, currentHeight, childHeightBeforeSnackbar);
        }
    }

    private void animateChildView(final View child, int currentHeight, int newHeight) {
        final ValueAnimator animator = ValueAnimator.ofInt(currentHeight, newHeight);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                child.getLayoutParams().height = (Integer) animator.getAnimatedValue();
                child.requestLayout();
            }
        });
        animator.setDuration(SNACKBAR_ANIMATION_DURATION);
        animator.start();
    }
}

package com.example.nbpmvvm.viewmodels;

import android.databinding.ObservableField;

import com.example.nbpmvvm.models.CurrencyRow;

/**
 * Created by Mikolaj D. on 17.10.2017.
 */

public class CurrencyRowViewModelImpl implements CurrencyRowViewModel {
    private ObservableField<CurrencyRow> currencyRow;
    private ObservableField<String> currencyTitle;

    public CurrencyRowViewModelImpl(CurrencyRow currencyRow){
        this.currencyRow = new ObservableField<>(currencyRow);
        this.currencyTitle = new ObservableField<>("text");
    }

    @Override
    public ObservableField<CurrencyRow> getCurrencyRow() {
        return currencyRow;
    }

    @Override
    public ObservableField<String> getCurrencyTitle() {
        return currencyTitle;
    }
}

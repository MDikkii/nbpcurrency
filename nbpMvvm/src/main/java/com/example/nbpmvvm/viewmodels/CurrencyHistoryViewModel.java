package com.example.nbpmvvm.viewmodels;

import android.arch.lifecycle.MutableLiveData;
import android.database.Observable;
import android.databinding.ObservableField;

import org.joda.time.LocalDate;

import java.util.Map;

/**
 * Created by Mikolaj D. on 09.10.2017.
 */

public interface CurrencyHistoryViewModel {
    MutableLiveData<Map<LocalDate, Double>> getHistoricData();

    MutableLiveData<Map<LocalDate, Double>> getChartData();

    ObservableField<String> getCurrencyCode();

    void loadData(String code);
}

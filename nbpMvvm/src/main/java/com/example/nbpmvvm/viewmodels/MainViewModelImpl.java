package com.example.nbpmvvm.viewmodels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.MutableLiveData;
import android.databinding.ObservableField;

import com.example.nbpmvvm.models.DayTable;
import com.example.nbpmvvm.models.interactors.CurrencyRatesInteractor;
import com.example.nbpmvvm.models.interactors.CurrencyRatesInteractorImpl;

import org.joda.time.LocalDate;

import static com.example.nbpmvvm.models.services.CurrencyRatesIntentService.HISTORIC_ACTION;
import static com.example.nbpmvvm.models.services.CurrencyRatesIntentService.LAST_AVAILABLE_ACTION;
import static com.example.nbpmvvm.models.services.CurrencyRatesIntentService.TODAY_ACTION;


/**
 * Created by Mikolaj D. on 06.10.2017.
 */

public class MainViewModelImpl extends AndroidViewModel implements MainViewModel {

    public static final String TOP_LABEL_DATE_FORMAT = "dd MMMM, yyyy";

    private CurrencyRatesInteractor currencyRatesInteractor;

    private ObservableField<String> serviceAction;
    private MutableLiveData<DayTable> table;
    private MutableLiveData<String> date;
    private MutableLiveData<Boolean> isRefreshPossible;
    private MutableLiveData<Boolean> isProgressShown;
    private MutableLiveData<Boolean> showToast;
    private MutableLiveData<Boolean> setUpSyncAlarm;

    public MainViewModelImpl(Application application) {
        super(application);
        currencyRatesInteractor = new CurrencyRatesInteractorImpl(application.getApplicationContext());

        initializeObservables();
    }

    private void initializeObservables() {
        serviceAction = new ObservableField<>();
        table = new MutableLiveData<>();
        date = new MutableLiveData<>();
        isRefreshPossible = new MutableLiveData<>();
        isProgressShown = new MutableLiveData<>();
        showToast = new MutableLiveData<>();
        setUpSyncAlarm = new MutableLiveData<>();
    }

    @Override
    public void loadData() {
        if (currencyRatesInteractor.isDatabaseEmpty()) {
            loadCurrencyData(HISTORIC_ACTION);
        }
        loadTodayData();
    }

    private void loadCurrencyData(String action) {

        isProgressShown.setValue(true);
        if (action.equals(serviceAction.get())) {
            serviceAction.notifyChange();
        } else {
            serviceAction.set(action);
        }
    }

    @Override
    public void todayTableReceived(DayTable data) {
        checkDataStatus();
        if (data != null) {
            updateData(data);
        } else {
            showToast.setValue(true);
            loadCurrencyData(LAST_AVAILABLE_ACTION);
        }
        isProgressShown.setValue(false);
    }

    @Override
    public void lastAvailableDayTableReceived(DayTable data) {
        if (data != null) {
            updateData(data);
        }
        isProgressShown.setValue(false);
    }

    @Override
    public void historicTablesReceived() {
        isProgressShown.setValue(false);
    }

    @Override
    public void checkTodayDataAvailability() {
        if (!new LocalDate(currencyRatesInteractor.getLastAvailableDate()).isEqual(LocalDate.now())) {
            loadTodayData();
            setUpSyncAlarm.setValue(true);
        }
    }

    private void checkDataStatus() {
        if (!new LocalDate(currencyRatesInteractor.getLastAvailableDate()).isEqual(LocalDate.now())) {
            isRefreshPossible.setValue(true);
        } else {
            isRefreshPossible.setValue(false);
        }
    }

    private void updateData(DayTable data) {
        table.setValue(data);
        date.setValue(new LocalDate(data.getEffectiveDate()).toString(TOP_LABEL_DATE_FORMAT));
    }

    @Override
    public ObservableField<String> getServiceAction() {
        return serviceAction;
    }

    @Override
    public void loadTodayData() {
        loadCurrencyData(TODAY_ACTION);
    }

    @Override
    public MutableLiveData<DayTable> getTable() {
        return table;
    }

    @Override
    public MutableLiveData<Boolean> getIsProgressShown() {
        return isProgressShown;
    }

    @Override
    public MutableLiveData<Boolean> getIsRefreshPossible() {
        return isRefreshPossible;
    }

    @Override
    public MutableLiveData<String> getDate() {
        return date;
    }

    @Override
    public MutableLiveData<Boolean> getShowToast() {
        return showToast;
    }

    public MutableLiveData<Boolean> getSetUpSyncAlarm() {
        return setUpSyncAlarm;
    }
}
package com.example.nbpmvvm.viewmodels;


import android.arch.lifecycle.MutableLiveData;
import android.databinding.ObservableField;

import com.example.nbpmvvm.models.DayTable;

/**
 * Created by Mikolaj D. on 09.10.2017.
 */

public interface MainViewModel {
    MutableLiveData<DayTable> getTable();

    MutableLiveData<Boolean> getIsProgressShown();

    MutableLiveData<Boolean> getIsRefreshPossible();

    MutableLiveData<String> getDate();

    void loadData();

    void todayTableReceived(DayTable data);

    void lastAvailableDayTableReceived(DayTable data);

    void historicTablesReceived();

    void checkTodayDataAvailability();

    ObservableField<String> getServiceAction();

    void loadTodayData();

    MutableLiveData<Boolean> getShowToast();

    MutableLiveData<Boolean> getSetUpSyncAlarm();
}

package com.example.nbpmvvm.viewmodels;

import android.databinding.ObservableField;

import com.example.nbpmvvm.models.CurrencyRow;

/**
 * Created by Mikolaj D. on 17.10.2017.
 */

public interface CurrencyRowViewModel {
    ObservableField<CurrencyRow> getCurrencyRow();

    ObservableField<String> getCurrencyTitle();
}

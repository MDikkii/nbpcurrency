package com.example.nbpmvvm.viewmodels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.content.Context;
import android.databinding.ObservableField;

import com.example.nbpmvvm.models.interactors.CurrencyRatesInteractor;
import com.example.nbpmvvm.models.interactors.CurrencyRatesInteractorImpl;

import org.joda.time.LocalDate;

import java.util.Comparator;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by Mikolaj D. on 06.10.2017.
 */

public class CurrencyHistoryViewModelImpl extends AndroidViewModel implements CurrencyHistoryViewModel {

    private CurrencyRatesInteractor currencyRatesInteractor;
    private MutableLiveData<Map<LocalDate, Double>> historicData;
    private MutableLiveData<Map<LocalDate, Double>> chartData;
    private ObservableField<String> currencyCode = new ObservableField<>();

    public CurrencyHistoryViewModelImpl(Application application) {
        super(application);
        currencyRatesInteractor = new CurrencyRatesInteractorImpl(application.getApplicationContext());
    }

    @Override
    public MutableLiveData<Map<LocalDate, Double>> getHistoricData() {
        if(historicData == null){
            historicData= new MutableLiveData<>();
        }
        return historicData;
    }

    @Override
    public MutableLiveData<Map<LocalDate, Double>> getChartData() {
        if(chartData == null){
            chartData= new MutableLiveData<>();
        }
        return chartData;
    }

    @Override
    public ObservableField<String> getCurrencyCode() {
        return currencyCode;
    }


    @Override
    public void loadData(String code) {
        Map<LocalDate, Double> historicTables = currencyRatesInteractor.getHistoricCurrencyTable(code);

        currencyCode.set(code);
        historicData.setValue(historicTables);
        chartData.setValue(getChartData(historicTables));
    }

    private Map<LocalDate,Double> getChartData(Map<LocalDate, Double> historicData) {
        Map<LocalDate, Double> chartData = new TreeMap<>(new Comparator<LocalDate>() {
            @Override
            public int compare(LocalDate localDate, LocalDate t1) {
                return localDate.compareTo(t1);
            }
        });

        chartData.putAll(historicData);

        return chartData;
    }
}
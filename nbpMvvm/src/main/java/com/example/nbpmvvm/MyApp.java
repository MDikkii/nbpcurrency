package com.example.nbpmvvm;

import android.app.Application;

import net.danlew.android.joda.JodaTimeAndroid;

/**
 * Created by Mikolaj D. on 03.10.2017.
 */

public class MyApp extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        JodaTimeAndroid.init(this);
    }
}
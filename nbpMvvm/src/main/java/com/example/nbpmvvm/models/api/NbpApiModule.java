package com.example.nbpmvvm.models.api;

/**
 * Created by Mikolaj D. on 10.10.2017.
 */

interface NbpApiModule {
    NbpApi createNbpApi();
}

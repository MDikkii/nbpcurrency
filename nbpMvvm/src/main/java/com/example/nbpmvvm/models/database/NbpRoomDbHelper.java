package com.example.nbpmvvm.models.database;

import android.arch.persistence.room.Room;
import android.content.Context;

import com.example.nbpmvvm.models.CurrencyRate;
import com.example.nbpmvvm.models.DayTable;

import org.joda.time.LocalDate;

import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by Mikolaj D. on 20.10.2017.
 */

public class NbpRoomDbHelper implements NbpDbHelper {
    AppRoomDatabase appdb;

    public NbpRoomDbHelper(Context context) {

        appdb = Room.databaseBuilder(context,
                AppRoomDatabase.class, "RoomDb3.db").fallbackToDestructiveMigration().allowMainThreadQueries().build();
    }

    @Override
    public long createDay(DayTable dayTable) {
        appdb.dayTableDao().insertDayTable(dayTable);
        addTodayValuesToCurrencyPerDay(dayTable.getRates(), dayTable.getEffectiveDate().getTime());
        return dayTable.getEffectiveDate().getTime();
    }


    @Override
    public DayTable getLastAvailableDay() {
        DayTable day = appdb.dayTableDao().getLatestDay();
        day.setRates(appdb.currencyRateDao().getCurrencyRatesForDay(day.getEffectiveDate()));

        return day;
    }

    @Override
    public DayTable getDay(Date date) {
        DayTable day = appdb.dayTableDao().getDay(date);
        if (day != null) {
            day.setRates(appdb.currencyRateDao().getCurrencyRatesForDay(day.getEffectiveDate()));
        }
        return day;
    }

    @Override
    public long createCurrency(CurrencyRate rate) {
        long id = appdb.currencyRateDao().insertCurrencyRate(rate);
        return id;
    }

    private void addTodayValuesToCurrencyPerDay(List<CurrencyRate> rates, long dayId) {
        for (CurrencyRate rate : rates) {
            rate.setDate(new Date(dayId));
            createCurrency(rate);
        }
    }

    @Override
    public Map<LocalDate, Double> getCurrencyHistory(String code) {
        Map<LocalDate, Double> result = new TreeMap<>(new Comparator<LocalDate>() {
            @Override
            public int compare(LocalDate localDate, LocalDate t1) {
                return t1.compareTo(localDate);
            }
        });

        List<CurrencyRate> list = appdb.currencyRateDao().getCurrencyHistory(code);

        for (CurrencyRate rate : list) {
            result.put(new LocalDate(rate.getDate()), rate.getValue());
        }

        return result;
    }

    @Override
    public boolean isEmpty() {
        long dayCounter = appdb.dayTableDao().countDayTableRows();
        return dayCounter == 0;
    }
}


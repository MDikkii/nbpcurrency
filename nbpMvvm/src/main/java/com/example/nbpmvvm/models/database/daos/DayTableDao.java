package com.example.nbpmvvm.models.database.daos;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.example.nbpmvvm.models.DayTable;

import java.util.Date;

/**
 * Created by Mikolaj D. on 19.10.2017.
 */
@Dao
public interface DayTableDao {
    @Insert
    public long insertDayTable(DayTable dayTable);

    @Query("SELECT * FROM dayTable " +
            "WHERE dayTable.effectiveDate = :date")
    public DayTable getDay(Date date);

    @Query("SELECT * FROM dayTable " +
            "ORDER BY effectiveDate " +
            "DESC LIMIT 1")
    public DayTable getLatestDay();

    @Query("SELECT COUNT(*) FROM dayTable")
    long countDayTableRows();
}

package com.example.nbpmvvm.models.api;

import com.example.nbpmvvm.models.DayTable;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;

/**
 * Created by Mikolaj D. on 10.10.2017.
 */

public interface NbpApi {

    @Headers("Accept: application/json")
    @GET("exchangerates/tables/A/{fromDate}/{toDate}")
    Call<List<DayTable>> getCurrencyTables(@Path("fromDate") String fromDate, @Path("toDate") String toDate);
}

package com.example.nbpmvvm.models.database.daos;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.example.nbpmvvm.models.CurrencyRate;

import java.util.Date;
import java.util.List;

/**
 * Created by Mikolaj D. on 19.10.2017.
 */
@Dao
public interface CurrencyRateDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    long insertCurrencyRate(CurrencyRate rate);

    @Query("SELECT * FROM currencyRate WHERE date IS :dayDate")
    List<CurrencyRate> getCurrencyRatesForDay(Date dayDate);

    @Query("SELECT * FROM currencyRate WHERE code IS :code")
    List<CurrencyRate> getCurrencyHistory(String code);
}

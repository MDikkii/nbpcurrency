package com.example.nbpmvvm.models.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;

import com.example.nbpmvvm.models.CurrencyRate;
import com.example.nbpmvvm.models.DayTable;
import com.example.nbpmvvm.models.database.daos.CurrencyRateDao;
import com.example.nbpmvvm.models.database.daos.DayTableDao;

/**
 * Created by Mikolaj D. on 19.10.2017.
 */
@Database(entities = {CurrencyRate.class, DayTable.class}, version = 2)
@TypeConverters({Converters.class})
public abstract class AppRoomDatabase extends RoomDatabase {
    public abstract CurrencyRateDao currencyRateDao();

    public abstract DayTableDao dayTableDao();
}
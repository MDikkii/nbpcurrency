package com.example.nbpmvvm.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by Mikolaj D. on 03.10.2017.
 */

@Entity(foreignKeys = @ForeignKey(entity = DayTable.class, parentColumns = "effectiveDate", childColumns = "date"))
public class CurrencyRate implements Parcelable {

    public static final Creator<CurrencyRate> CREATOR = new Creator<CurrencyRate>() {
        @Override
        public CurrencyRate createFromParcel(Parcel in) {
            return new CurrencyRate(in);
        }

        @Override
        public CurrencyRate[] newArray(int size) {
            return new CurrencyRate[size];
        }
    };
    @PrimaryKey(autoGenerate = true)
    private long id;
    @SerializedName("currency")
    private String name;
    private String code;
    @SerializedName("mid")
    private double value;
    private Date date;
    protected CurrencyRate(Parcel parcel) {
        name = parcel.readString();
        code = parcel.readString();
        value = parcel.readDouble();
        date = new Date(parcel.readLong());
    }
    public CurrencyRate() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(name);
        parcel.writeString(code);
        parcel.writeDouble(value);
        parcel.writeLong(date.getTime());
    }
}
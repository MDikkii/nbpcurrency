package com.example.nbpmvvm.models.services;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;

import com.example.nbpmvvm.models.DayTable;
import com.example.nbpmvvm.models.interactors.CurrencyRatesInteractor;
import com.example.nbpmvvm.models.interactors.CurrencyRatesInteractorImpl;

import java.util.ArrayList;


import static com.example.nbpmvvm.models.services.CurrencyRatesResultReceiver.PARAM_RESULT;
import static com.example.nbpmvvm.models.services.CurrencyRatesResultReceiver.PARCELABLE_ARRAY_RESULT_CODE;
import static com.example.nbpmvvm.models.services.CurrencyRatesResultReceiver.PARCELABLE_RESULT_CODE;
import static com.example.nbpmvvm.models.services.CurrencyRatesResultReceiver.RESULT_RECIVER;

/**
 * Created by Mikolaj D. on 09.10.2017.
 */

public class CurrencyRatesIntentService extends IntentService {
    public static final String TODAY_ACTION = "TODAY";
    public static final String HISTORIC_ACTION = "HISTORIC";
    public static final String LAST_AVAILABLE_ACTION = "LAST_AVAILABLE";

    public CurrencyRatesIntentService() {
        super("CurrencyRatesIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        String action = intent.getAction();
        ResultReceiver resultReceiver = intent.getParcelableExtra(RESULT_RECIVER);
        CurrencyRatesInteractor currencyRatesInteractor = new CurrencyRatesInteractorImpl(this);

        if (action == TODAY_ACTION) {
            handleTodayAction(resultReceiver, currencyRatesInteractor);
        }

        if (action == LAST_AVAILABLE_ACTION) {
            handleLastAvailableAction(resultReceiver, currencyRatesInteractor);
        }

        if (action == HISTORIC_ACTION) {
            handleHistoricAction(resultReceiver, currencyRatesInteractor);
        }
    }

    private void handleHistoricAction(final ResultReceiver resultReceiver, CurrencyRatesInteractor currencyRatesInteractor) {
        ArrayList<DayTable> tables = new ArrayList<>(currencyRatesInteractor.getHistoricTables());

        Bundle resultBundle = new Bundle();

        resultBundle.putParcelableArrayList(PARAM_RESULT, tables);
        resultReceiver.send(PARCELABLE_ARRAY_RESULT_CODE, resultBundle);
    }

    private void handleLastAvailableAction(ResultReceiver resultReceiver, CurrencyRatesInteractor currencyRatesInteractor) {
        Bundle resultBundle = new Bundle();
        DayTable table = currencyRatesInteractor.getLastAvailableTable();

        resultBundle.putParcelable(PARAM_RESULT, table);
        resultReceiver.send(PARCELABLE_RESULT_CODE, resultBundle);
    }

    private void handleTodayAction(final ResultReceiver resultReceiver, CurrencyRatesInteractor currencyRatesInteractor) {

        DayTable todayTable = currencyRatesInteractor.getTodayTable();

        Bundle resultBundle = new Bundle();

        resultBundle.putParcelable(PARAM_RESULT, todayTable);
        resultReceiver.send(PARCELABLE_RESULT_CODE, resultBundle);
    }

//    public interface CurrencyRatesInteractorCallback<T> {
//        void onSuccess(T result);
//        void onFail();
//    }
}


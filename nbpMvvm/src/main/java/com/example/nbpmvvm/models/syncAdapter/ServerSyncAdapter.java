package com.example.nbpmvvm.models.syncAdapter;

import android.accounts.Account;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SyncResult;
import android.os.Bundle;
import android.util.Log;


import com.example.nbpmvvm.models.DayTable;
import com.example.nbpmvvm.models.interactors.CurrencyRatesInteractor;
import com.example.nbpmvvm.models.interactors.CurrencyRatesInteractorImpl;

import java.util.Calendar;

/**
 * Created by Mikolaj D. on 13.10.2017.
 */

public class ServerSyncAdapter extends AbstractThreadedSyncAdapter {
    CurrencyRatesInteractor currencyRatesInteractor;
    public static final String AUTHORITY = "com.example.demkowmik.nbpcurrencyviewer.provider";

    public ServerSyncAdapter(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
        currencyRatesInteractor = new CurrencyRatesInteractorImpl(context);
    }

    public ServerSyncAdapter(Context context, boolean autoInitialize, boolean allowParallelSyncs) {
        super(context, autoInitialize, allowParallelSyncs);
        currencyRatesInteractor = new CurrencyRatesInteractorImpl(context);
    }

    @Override
    public void onPerformSync(Account account, Bundle bundle, String s, ContentProviderClient contentProviderClient, SyncResult syncResult) {
        Log.e("Sync", "onPerformSync");
        DayTable table = currencyRatesInteractor.getTodayTable();
        if (table != null) {
            ContentResolver.removePeriodicSync(account, AUTHORITY, Bundle.EMPTY);

            setUpNextAlarm();
        }
    }

    private void setUpNextAlarm() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        Log.i("Hour", String.valueOf(calendar.get(Calendar.HOUR_OF_DAY)));
        calendar.set(Calendar.HOUR_OF_DAY, 12);
        calendar.set(Calendar.MINUTE, 0);
        if (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.FRIDAY) {
            calendar.add(Calendar.DATE, 3);
        } else {
            calendar.add(Calendar.DATE, 1);
        }

        AlarmManager alarmMenager = (AlarmManager) getContext().getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(getContext(), AlarmReceicer.class);
        intent.setAction("com.example.demkowmik.nbpcurrencyviewer.alarms");
        PendingIntent alarmIntent = PendingIntent.getBroadcast(getContext(), 0, intent, 0);
        alarmMenager.set(AlarmManager.RTC, calendar.getTimeInMillis(), alarmIntent);
    }
}
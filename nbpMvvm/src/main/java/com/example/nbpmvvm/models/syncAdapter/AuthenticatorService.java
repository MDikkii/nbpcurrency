package com.example.nbpmvvm.models.syncAdapter;


import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

/**
 * Created by Mikolaj D. on 13.10.2017.
 */

public class AuthenticatorService extends Service {
    // Instance field that stores the authenticator object
    private StubAuthenticator mAuthenticator;
    @Override
    public void onCreate() {
        // Create a new authenticator object
        mAuthenticator = new StubAuthenticator(this);
    }
    /*
     * When the system binds to this Service to make the RPC call
     * return the authenticator's IBinder.
     */
    @Override
    public IBinder onBind(Intent intent) {
        return mAuthenticator.getIBinder();
    }
}

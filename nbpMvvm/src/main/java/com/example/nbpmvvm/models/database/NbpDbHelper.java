package com.example.nbpmvvm.models.database;

import com.example.nbpmvvm.models.CurrencyRate;
import com.example.nbpmvvm.models.DayTable;

import org.joda.time.LocalDate;

import java.util.Date;
import java.util.Map;

/**
 * Created by Mikolaj D. on 20.10.2017.
 */

public interface NbpDbHelper {
    long createDay(DayTable dayTable);

    DayTable getLastAvailableDay();

    DayTable getDay(Date date);

    long createCurrency(CurrencyRate rate);

    Map<LocalDate, Double> getCurrencyHistory(String code);

    boolean isEmpty();
}

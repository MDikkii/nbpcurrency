package com.example.nbpmvvm.models;

/**
 * Created by Mikolaj D. on 17.10.2017.
 */

public class CurrencyRow {
    private String currencyRowTitle;
    private String currencyRowValue;

    public CurrencyRow(String title, String value) {
        currencyRowTitle = title;
        currencyRowValue = value;
    }

    public String getCurrencyRowTitle() {
        return currencyRowTitle;
    }

    public String getCurrencyRowValue() {
        return currencyRowValue;
    }
}

package com.example.nbpmvvm.models.interactors;


import com.example.nbpmvvm.models.DayTable;

import org.joda.time.LocalDate;

import java.util.List;
import java.util.Map;

/**
 * Created by Mikolaj D. on 06.10.2017.
 */
public interface CurrencyRatesInteractor {
    List<DayTable> getHistoricTables();

    DayTable getTodayTable();

    DayTable getLastAvailableTable();

    Map<LocalDate, Double> getHistoricCurrencyTable(String code);

    boolean isDatabaseEmpty();

    long getLastAvailableDate();
}
package com.example.nbpmvvm.models.syncAdapter;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import static android.content.Context.ACCOUNT_SERVICE;

/**
 * Created by Mikolaj D. on 13.10.2017.
 */

public class AlarmReceicer extends BroadcastReceiver {

    public static final String AUTHORITY = "com.example.demkowmik.nbpcurrencyviewer.provider";
    // An account type, in the form of a domain name
    public static final String ACCOUNT_TYPE = "com.example.demkowmik.nbpcurrencyviewer.account";
    // The account name
    public static final String ACCOUNT = "User account";

    @Override
    public void onReceive(Context context, Intent intent) {
        Account account = createSyncAccount(context);
        if (ContentResolver.getPeriodicSyncs(account, AUTHORITY).isEmpty()) {
            ContentResolver.addPeriodicSync(account, AUTHORITY, Bundle.EMPTY, 60 * 30);
        }

        Log.i("Alarm", "alarm done");
    }

    public Account createSyncAccount(Context context) {
        Account newAccount = new Account(
                ACCOUNT, ACCOUNT_TYPE);

        AccountManager accountManager =
                (AccountManager) context.getSystemService(
                        ACCOUNT_SERVICE);

        if (accountManager.addAccountExplicitly(newAccount, null, null)) {
            ContentResolver.setSyncAutomatically(newAccount, AUTHORITY, true);
            Log.i("User account", "Created");
        } else {
            Log.i("User account", "Present");
        }
        return newAccount;
    }
}

package com.example.nbpmvvm.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Mikolaj D. on 03.10.2017.
 */

@Entity
public class DayTable implements Parcelable {
    public static final Creator<DayTable> CREATOR = new Creator<DayTable>() {
        @Override
        public DayTable createFromParcel(Parcel in) {
            return new DayTable(in);
        }

        @Override
        public DayTable[] newArray(int size) {
            return new DayTable[size];
        }
    };
    @SerializedName("table")
    private String tableType;
    @PrimaryKey
    private Date effectiveDate;


    @Ignore
    private List<CurrencyRate> rates;

    public DayTable() {
        rates = new ArrayList<>();
    }

    protected DayTable(Parcel parcel) {
        tableType = parcel.readString();
        effectiveDate = new Date(parcel.readLong());
        rates = parcel.createTypedArrayList(CurrencyRate.CREATOR);
    }

    public String getTableType() {
        return tableType;
    }

    public void setTableType(String tableType) {
        this.tableType = tableType;
    }

    public Date getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(Date effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public List<CurrencyRate> getRates() {
        return rates;
    }

    public void setRates(List<CurrencyRate> rates) {
        this.rates = rates;
        for (CurrencyRate r : rates) {
            r.setDate(getEffectiveDate());
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(tableType);
        parcel.writeLong(effectiveDate.getTime());
        parcel.writeTypedList(rates);
    }
}
package com.example.demkowmik.nbpcurrencyviewer;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.net.Uri;
import android.util.Log;

import com.example.demkowmik.nbpcurrencyviewer.models.CurrencyRate;
import com.example.demkowmik.nbpcurrencyviewer.models.DayTable;
import com.example.demkowmik.nbpcurrencyviewer.models.interactors.CurrencyRatesInteractor;
import com.example.demkowmik.nbpcurrencyviewer.models.interactors.CurrencyRatesInteractorImpl;
import com.example.demkowmik.nbpcurrencyviewer.models.interactors.NbpApi;
import com.example.demkowmik.nbpcurrencyviewer.models.database.NbpDbHelper;

import net.danlew.android.joda.JodaTimeAndroid;

import org.joda.time.LocalDate;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.robolectric.RobolectricTestRunner;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(RobolectricTestRunner.class)
@PrepareForTest({Log.class, Uri.Builder.class})
public class CurrencyRatesInteractorTestDto {

    @InjectMocks
    CurrencyRatesInteractor currencyRatesInteractor;

    @Mock
    Activity activity;

    @Mock
    NbpDbHelper nbpDbHelper;

    @Mock
    NbpApi nbpApi;


    @Before
    public void setUp() throws Exception {
        currencyRatesInteractor = new CurrencyRatesInteractorImpl(activity);
        Context context = mock(Context.class);
        Context appContext = mock(Context.class);
        Resources resources = mock(Resources.class);
        when(resources.openRawResource(anyInt())).thenReturn(mock(InputStream.class));
        when(appContext.getResources()).thenReturn(resources);
        when(context.getApplicationContext()).thenReturn(appContext);
        JodaTimeAndroid.init(context);
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getHistoricTableWithSuccess() {
        HashMap<LocalDate, Double> hashMap = new HashMap<>();
        hashMap.put(new LocalDate(), 2.223);

        when(nbpDbHelper.getCurrencyHistory("USD")).thenReturn(hashMap);


        Map<LocalDate, Double> result = currencyRatesInteractor.getHistoricCurrencyTable("USD");
        Assert.assertEquals(1, result.size());
        Assert.assertTrue(result.containsKey(new LocalDate()));
        Assert.assertTrue(result.containsValue(2.223));
    }

    @Test
    public void getValidTodayTableFromDB() {
        DayTable ct = new DayTable();
        ct.setTableType("B");
        ct.setEffectiveDate(LocalDate.now().toDate());
        ct.setRates(new ArrayList<CurrencyRate>());

        when(nbpDbHelper.getDay(LocalDate.now().toDate())).thenReturn(ct);

        DayTable result = currencyRatesInteractor.getTodayTable();

        Assert.assertEquals("B", result.getTableType());
        Assert.assertEquals(LocalDate.now().toDate(), result.getEffectiveDate());
        Assert.assertEquals(0, result.getRates().size());
    }

    @Test
    public void getValidTodayTableFromServerWhenDbReturnsNull() throws IOException {
        List<DayTable> ctList = new ArrayList<>();
        DayTable ct = new DayTable();
        ct.setTableType("B");
        ct.setEffectiveDate(LocalDate.now().toDate());
        ct.setRates(new ArrayList<CurrencyRate>());

        ctList.add(ct);

        Call<List<DayTable>> call = setUpCallWithSuccessResponse(ctList);
        when(nbpApi.getCurrencyTables(Mockito.anyString(), Mockito.anyString())).thenReturn(call);
        when(nbpDbHelper.getDay(LocalDate.now().toDate())).thenReturn(null);

        DayTable result = currencyRatesInteractor.getTodayTable();

        Assert.assertEquals("B", result.getTableType());
        Assert.assertEquals(LocalDate.now().toDate(), result.getEffectiveDate());
        Assert.assertEquals(0, result.getRates().size());
    }

    @Test
    public void getNullWhenDbAndServerReturnNull() throws IOException {

        when(nbpDbHelper.getDay(LocalDate.now().toDate())).thenReturn(null);
        Call<List<DayTable>> call = setUpCallWithNullResponse();
        when(nbpApi.getCurrencyTables(Mockito.anyString(), Mockito.anyString())).thenReturn(call);

        DayTable result = currencyRatesInteractor.getTodayTable();

        Assert.assertNull(result);
    }

    private Call<List<DayTable>> setUpCallWithSuccessResponse(List<DayTable> ctList) {
        final Response<List<DayTable>> response = Response.success(ctList);
        return new Call<List<DayTable>>() {
            @Override
            public Response<List<DayTable>> execute() throws IOException {
                return response;
            }

            @Override
            public void enqueue(Callback<List<DayTable>> callback) {

            }

            @Override
            public boolean isExecuted() {
                return false;
            }

            @Override
            public void cancel() {

            }

            @Override
            public boolean isCanceled() {
                return false;
            }

            @Override
            public Call<List<DayTable>> clone() {
                return null;
            }

            @Override
            public Request request() {
                return null;
            }
        };
    }

    private Call<List<DayTable>> setUpCallWithNullResponse() {
        return new Call<List<DayTable>>() {
            @Override
            public Response<List<DayTable>> execute() throws IOException {
                return null;
            }

            @Override
            public void enqueue(Callback<List<DayTable>> callback) {

            }

            @Override
            public boolean isExecuted() {
                return false;
            }

            @Override
            public void cancel() {

            }

            @Override
            public boolean isCanceled() {
                return false;
            }

            @Override
            public Call<List<DayTable>> clone() {
                return null;
            }

            @Override
            public Request request() {
                return null;
            }
        };
    }
}
package com.example.demkowmik.nbpcurrencyviewer.models.database;

import com.raizlabs.android.dbflow.annotation.Database;

/**
 * Created by Mikolaj D. on 17.10.2017.
 */

@Database(version = AppDatabase.VERSION, name = AppDatabase.NAME)
public class AppDatabase {

    public static final int VERSION = 1;
    public static final String NAME = "NbpDb2";
}

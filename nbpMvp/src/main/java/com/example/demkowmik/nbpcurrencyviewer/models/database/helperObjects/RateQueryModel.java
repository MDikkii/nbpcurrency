package com.example.demkowmik.nbpcurrencyviewer.models.database.helperObjects;

import com.example.demkowmik.nbpcurrencyviewer.models.database.AppDatabase;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.QueryModel;
import com.raizlabs.android.dbflow.structure.BaseQueryModel;

/**
 * Created by Mikolaj D. on 18.10.2017.
 */

@QueryModel(database = AppDatabase.class)
public class RateQueryModel extends BaseQueryModel {
    @Column
    String name;
    @Column
    String code;
    @Column
    double value;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }
}

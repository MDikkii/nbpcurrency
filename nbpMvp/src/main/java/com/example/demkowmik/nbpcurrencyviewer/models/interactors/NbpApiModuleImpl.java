package com.example.demkowmik.nbpcurrencyviewer.models.interactors;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Mikolaj D. on 10.10.2017.
 */

public class NbpApiModuleImpl implements NbpApiModule {
    private static final String BASE_URL = "http://api.nbp.pl/api/";
    private static final String YEAR_MONTH_DAY_FORMAT = "yyyy-MM-dd";

    private Retrofit getRetrofit() {
       return new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(getGson()))
                .build();
    }

    private Gson getGson() {
        return new GsonBuilder()
                .setLenient()
                .setDateFormat(YEAR_MONTH_DAY_FORMAT)
                .create();
    }

    @Override
    public NbpApi createNbpApi(){
        return getRetrofit().create(NbpApi.class);
    }
}

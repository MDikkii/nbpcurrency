package com.example.demkowmik.nbpcurrencyviewer.presenters;

import android.content.Context;

import com.example.demkowmik.nbpcurrencyviewer.models.DayTable;
import com.example.demkowmik.nbpcurrencyviewer.models.interactors.CurrencyRatesInteractor;
import com.example.demkowmik.nbpcurrencyviewer.models.interactors.CurrencyRatesInteractorImpl;
import com.example.demkowmik.nbpcurrencyviewer.views.MainView;

import org.joda.time.LocalDate;

import java.lang.ref.WeakReference;

import static com.example.demkowmik.nbpcurrencyviewer.models.services.CurrencyRatesIntentService.HISTORIC_ACTION;
import static com.example.demkowmik.nbpcurrencyviewer.models.services.CurrencyRatesIntentService.LAST_AVAILABLE_ACTION;
import static com.example.demkowmik.nbpcurrencyviewer.models.services.CurrencyRatesIntentService.TODAY_ACTION;

/**
 * Created by demkow.mik on 06.10.2017.
 */

public class MainPresenterImpl implements MainPresenter {

    private WeakReference<MainView> mainView;
    private CurrencyRatesInteractor currencyRatesInteractor;

    public MainPresenterImpl(MainView view, Context context) {
        mainView = new WeakReference<>(view);
        currencyRatesInteractor = new CurrencyRatesInteractorImpl(context);
    }

    private MainView getView() throws NullPointerException {
        if (mainView != null)
            return mainView.get();
        else
            throw new NullPointerException("View is unavailable");
    }

    @Override
    public void loadData() {
        if (currencyRatesInteractor.isDatabaseEmpty()) {
            loadHistoricData();
        }
        loadTodayData();
    }

    @Override
    public void loadTodayData() {
        getView().showProgress();
        getView().startCurrencyRatesIntentService(TODAY_ACTION);//TODAY_ACTION
    }


    private void loadHistoricData() {
        getView().showProgress();
        getView().startCurrencyRatesIntentService(HISTORIC_ACTION);
    }

    @Override
    public void showLastAvailableTable() {
        getView().showProgress();
        getView().startCurrencyRatesIntentService(LAST_AVAILABLE_ACTION);
    }

    @Override
    public void todayTableReceived(DayTable data) {
        checkDataStatus();
        if (data != null) {
            sendDataToView(data);
        } else {
            getView().showOldDataToast();
            showLastAvailableTable();
        }
        getView().hideProgress();
    }

    @Override
    public void lastAvailableDayTableReceived(DayTable data) {
        if (data != null) {
            sendDataToView(data);
        }
        getView().hideProgress();
    }

    @Override
    public void historicTablesReceived() {
        getView().hideProgress();
    }

    @Override
    public void checkTodayDataAvailability() {
        if (!new LocalDate(currencyRatesInteractor.getLastAvailableDate()).isEqual(LocalDate.now())) {
            loadTodayData();
            getView().setUpAlarmForSync();
        }
    }

    private void checkDataStatus() {
        if (!new LocalDate(currencyRatesInteractor.getLastAvailableDate()).isEqual(LocalDate.now())) {
            getView().showOldDataStatus();
        } else {
            getView().hideOldDataStatus();
        }
    }

    private void sendDataToView(DayTable data) {
        getView().setDate(new LocalDate(data.getEffectiveDate()));
        getView().setOneDayTableSource(data);
    }
}
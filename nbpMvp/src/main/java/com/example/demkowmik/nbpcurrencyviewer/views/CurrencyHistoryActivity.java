package com.example.demkowmik.nbpcurrencyviewer.views;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import com.example.demkowmik.nbpcurrencyviewer.R;
import com.example.demkowmik.nbpcurrencyviewer.presenters.CurrencyHistoryPresenter;
import com.example.demkowmik.nbpcurrencyviewer.presenters.CurrencyHistoryPresenterImpl;
import com.example.demkowmik.nbpcurrencyviewer.views.adapters.HistoricCurrencyAdapter;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;

import org.joda.time.LocalDate;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class CurrencyHistoryActivity extends Activity implements CurrencyHistoryView {

    public static final String CODE = "code";
    public static final String DEFAULT_CODE_USD = "USD";
    public static final String DAY_MONTH_SHORT_FORMAT = "dd-MM";
    private String code;
    RecyclerView recyclerView;

    private CurrencyHistoryPresenter currencyHistoryPresenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_currency);
        currencyHistoryPresenter = new CurrencyHistoryPresenterImpl(this, this);
        recyclerView = findViewById(R.id.singleCurrencyRecyclerView);
        code = getCode();
        setUpScreenTitle(code);
        setUpRecyclerView();

        currencyHistoryPresenter.loadData(code);
    }

    private void setUpRecyclerView() {
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
    }

    private void setUpScreenTitle(String title) {
        TextView codeTextView = findViewById(R.id.singleCurrencyCodeTextView);
        codeTextView.setText(title);
    }

    @Override
    public void setUpChart(Map<LocalDate, Double> chartData) {

        if (chartData.size() > 0) { // dealing with library bug when there is empty dataSet
            LineChart chart = findViewById(R.id.chart);
            setUpXAxis(chart);

            chart.setData(getLineData(code, chartData));

            Description description = new Description();
            description.setText("");

            chart.setDescription(description);

            chart.invalidate(); // refresh
        }
    }

    private void setUpXAxis(LineChart chart) {
        XAxis xAxis = chart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                BigDecimal bd = BigDecimal.valueOf(value);
                return new LocalDate(bd.longValue()).toString(DAY_MONTH_SHORT_FORMAT);
            }
        });
    }

    public String getCode() {
        Intent intent = getIntent();
        String code = intent.getStringExtra(CODE);

        if (code.isEmpty()) {
            code = DEFAULT_CODE_USD;
        }

        return code;
    }

    @Override
    public void setUpTableSource(Map<LocalDate, Double> historicData) {
        recyclerView.setAdapter(new HistoricCurrencyAdapter(this, historicData));
    }

    @NonNull
    private LineDataSet getDataSet(String code, List<Entry> entries) {
        LineDataSet dataSet = new LineDataSet(entries, code);

        dataSet.setDrawCircles(false);
        dataSet.setDrawValues(false);
        dataSet.setDrawFilled(true);

        dataSet.setLineWidth(5f);

        dataSet.setColor(ContextCompat.getColor(this, R.color.primaryDarkColor));
        dataSet.setFillColor(ContextCompat.getColor(this, R.color.primaryColor));
        dataSet.setMode(LineDataSet.Mode.CUBIC_BEZIER);

        return dataSet;
    }

    private LineData getLineData(String code, Map<LocalDate, Double> chartData) {
        List<Entry> entries = getEntries(chartData);
        LineDataSet dataSet = getDataSet(code, entries);
        return new LineData(dataSet);
    }

    @NonNull
    private List<Entry> getEntries(Map<LocalDate, Double> chartData) {
        List<Entry> entries = new ArrayList<>();
        for (Map.Entry<LocalDate, Double> mapItem : chartData.entrySet()) {
            entries.add(new Entry((float) mapItem.getKey().toDate().getTime(), (float) (double) mapItem.getValue()));//, (float)(double)mapItem.getValue()
        }
        return entries;
    }
}
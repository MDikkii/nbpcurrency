package com.example.demkowmik.nbpcurrencyviewer.models;

import com.example.demkowmik.nbpcurrencyviewer.models.CurrencyRate;
import com.example.demkowmik.nbpcurrencyviewer.models.DayTable;
import com.example.demkowmik.nbpcurrencyviewer.models.database.AppDatabase;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.ForeignKey;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

/**
 * Created by Mikolaj D. on 18.10.2017.
 */
@Table(database = AppDatabase.class)
public class SingleRate extends BaseModel {
    @Column
    public double value;
    @ForeignKey
    public CurrencyRate currency;
    @ForeignKey
    public DayTable dayTable;
    @PrimaryKey(autoincrement = true)
    long currencyRateId;
}

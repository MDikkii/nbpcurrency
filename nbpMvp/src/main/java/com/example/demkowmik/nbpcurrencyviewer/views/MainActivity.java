package com.example.demkowmik.nbpcurrencyviewer.views;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BaseTransientBottomBar;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.demkowmik.nbpcurrencyviewer.R;
import com.example.demkowmik.nbpcurrencyviewer.models.DayTable;
import com.example.demkowmik.nbpcurrencyviewer.models.services.CurrencyRatesIntentService;
import com.example.demkowmik.nbpcurrencyviewer.models.services.CurrencyRatesResultReceiver;
import com.example.demkowmik.nbpcurrencyviewer.models.syncAdapter.AlarmReceicer;
import com.example.demkowmik.nbpcurrencyviewer.presenters.MainPresenter;
import com.example.demkowmik.nbpcurrencyviewer.presenters.MainPresenterImpl;
import com.example.demkowmik.nbpcurrencyviewer.views.adapters.DailyCurrencyAdapter;

import org.joda.time.LocalDate;

import java.lang.ref.WeakReference;
import java.util.Calendar;
import java.util.List;

import static com.example.demkowmik.nbpcurrencyviewer.models.services.CurrencyRatesIntentService.HISTORIC_ACTION;
import static com.example.demkowmik.nbpcurrencyviewer.models.services.CurrencyRatesIntentService.LAST_AVAILABLE_ACTION;
import static com.example.demkowmik.nbpcurrencyviewer.models.services.CurrencyRatesIntentService.TODAY_ACTION;
import static com.example.demkowmik.nbpcurrencyviewer.models.services.CurrencyRatesResultReceiver.RESULT_RECIVER;

public class MainActivity extends AppCompatActivity implements MainView {
    public static final String CODE = "code";
    public static final String TOP_LABEL_DATE_FORMAT = "dd MMMM, yyyy";


    MainPresenter mainPresenter;
    private RecyclerView oneDayCurrencyRecyclerView;
    private SwipeRefreshLayout swipeRefreshLayout;
    Snackbar snackbar;
    private boolean isRefreshPossible;
    private boolean isSnackbarDismissed;
    private boolean wasOnCreateCalled;
    private boolean isSnackbarShown;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar myToolbar = findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);

        oneDayCurrencyRecyclerView = findViewById(R.id.oneDayCurrencyRecyclerView);
        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);

        setUpSwipeRefreshLayout();
        setUpCurrencyTable();

        mainPresenter = new MainPresenterImpl(this, this);
        mainPresenter.loadData();

        wasOnCreateCalled = true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        isSnackbarDismissed = false;
        if (!wasOnCreateCalled) {
            mainPresenter.checkTodayDataAvailability();
        }
        wasOnCreateCalled = false;

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_activity_menu, menu);

        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        MenuItem item = menu.findItem(R.id.refresh);

        if (isRefreshPossible) {
            item.setEnabled(true);
            item.setIcon(R.drawable.ic_refresh);
            item.getIcon().setAlpha(255);
        } else {
            item.setEnabled(false);
            item.setIcon(R.drawable.ic_current_data);
            item.getIcon().setAlpha(120);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.refresh:
                mainPresenter.loadTodayData();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setUpSnackbar() {
        snackbar = Snackbar
                .make(findViewById(R.id.mainLayout), R.string.data_unavailable, Snackbar.LENGTH_INDEFINITE);

        snackbar.setAction("Refresh", new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isSnackbarDismissed = true;
                snackbar.dismiss();
                mainPresenter.loadTodayData();
            }
        });

        snackbar.addCallback(getSnackbarCallback());
    }

    @NonNull
    private BaseTransientBottomBar.BaseCallback<Snackbar> getSnackbarCallback() {
        return new BaseTransientBottomBar.BaseCallback<Snackbar>() {
            @Override
            public void onDismissed(Snackbar transientBottomBar, int event) {
                super.onDismissed(transientBottomBar, event);
                isSnackbarDismissed = true;
                isSnackbarShown = false;
            }

            @Override
            public void onShown(Snackbar transientBottomBar) {
                super.onShown(transientBottomBar);
                isSnackbarShown = true;
                isSnackbarDismissed = false;
            }
        };
    }

    private void setUpCurrencyTable() {
        oneDayCurrencyRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        oneDayCurrencyRecyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
    }

    private void setUpSwipeRefreshLayout() {
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mainPresenter.loadTodayData();
            }
        });

        swipeRefreshLayout.setColorSchemeResources(
                R.color.secondaryColor,
                R.color.secondaryLightColor,
                R.color.secondaryDarkColor);
    }

    @Override
    public void showProgress() {
        swipeRefreshLayout.setRefreshing(true);
    }

    @Override
    public void hideProgress() {
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void setOneDayTableSource(DayTable dayTable) {
        oneDayCurrencyRecyclerView.setAdapter(new DailyCurrencyAdapter(this, dayTable));
    }

    @Override
    public void setDate(LocalDate date) {
        TextView dateTextView = findViewById(R.id.dateTextView);
        dateTextView.setText(date.toString(TOP_LABEL_DATE_FORMAT));
    }

    @Override
    public void startCurrencyRatesIntentService(String action) {
        CurrencyRatesResultReceiver currencyRatesResultReceiver = new CurrencyRatesResultReceiver(new Handler(this.getMainLooper()));

        if (action == TODAY_ACTION) {
            currencyRatesResultReceiver.setReceiver(new TodayTableResultReceiverCallback(mainPresenter));

        } else if (action == LAST_AVAILABLE_ACTION) {
            currencyRatesResultReceiver.setReceiver(new LastAvailableTableResultReceiverCallback(mainPresenter));
        } else if (action == HISTORIC_ACTION) {
            currencyRatesResultReceiver.setReceiver(new HistoricResultReceiverCallBack(mainPresenter));
        }

        Intent intent = new Intent(this, CurrencyRatesIntentService.class);
        intent.setAction(action);
        intent.putExtra(RESULT_RECIVER, currencyRatesResultReceiver);
        startService(intent);
    }

    @Override
    public void showOldDataStatus() {
        isRefreshPossible = true;
        swipeRefreshLayout.setEnabled(true);

        if (!isSnackbarDismissed && !isSnackbarShown) {
            setUpSnackbar();
            snackbar.show();
        }
        invalidateOptionsMenu();
    }

    @Override
    public void hideOldDataStatus() {
        isRefreshPossible = false;
        swipeRefreshLayout.setEnabled(false);
        if (isSnackbarShown) {
            snackbar.dismiss();
        }
        invalidateOptionsMenu();
    }

    @Override
    public void showOldDataToast() {
        if (isSnackbarDismissed) {
            Toast.makeText(this, R.string.no_data, Toast.LENGTH_SHORT).show();
        }
    }

    private static class TodayTableResultReceiverCallback implements CurrencyRatesResultReceiver.ResultReceiverCallBack<DayTable> {
        private WeakReference<MainPresenter> presenterWeakReference;

        TodayTableResultReceiverCallback(MainPresenter presenter) {
            presenterWeakReference = new WeakReference<>(presenter);
        }

        @Override
        public void onDataReceive(DayTable data) {
            Log.i("SingleDayReceiver", "onDataReceive");
            presenterWeakReference.get().todayTableReceived(data);
        }
    }

    private static class LastAvailableTableResultReceiverCallback implements CurrencyRatesResultReceiver.ResultReceiverCallBack<DayTable> {
        private WeakReference<MainPresenter> presenterWeakReference;

        LastAvailableTableResultReceiverCallback(MainPresenter presenter) {
            presenterWeakReference = new WeakReference<>(presenter);
        }

        @Override
        public void onDataReceive(DayTable data) {
            Log.i("SingleDayReceiver", "onDataReceive");
            presenterWeakReference.get().lastAvailableDayTableReceived(data);
        }
    }

    private class HistoricResultReceiverCallBack implements CurrencyRatesResultReceiver.ResultReceiverCallBack<List<DayTable>> {
        private WeakReference<MainPresenter> presenterWeakReference;

        HistoricResultReceiverCallBack(MainPresenter presenter) {
            presenterWeakReference = new WeakReference<>(presenter);
        }

        @Override
        public void onDataReceive(List<DayTable> data) {
            Log.i("HistoricReceiver", "onDataReceive");
            presenterWeakReference.get().historicTablesReceived();
        }
    }

    public void setUpAlarmForSync() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.set(Calendar.HOUR_OF_DAY, 12);
        calendar.set(Calendar.MINUTE, 0);

        AlarmManager alarmMenager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(this, AlarmReceicer.class);
        intent.setAction("com.example.demkowmik.nbpcurrencyviewer.alarms");
        PendingIntent alarmIntent = PendingIntent.getBroadcast(this, 0, intent, 0);
        alarmMenager.set(AlarmManager.RTC, calendar.getTimeInMillis(), alarmIntent);
    }
}
package com.example.demkowmik.nbpcurrencyviewer.models.services;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

/**
 * Created by Mikolaj D. on 09.10.2017.
 */

public class CurrencyRatesResultReceiver<T> extends ResultReceiver {
    public static final String PARAM_RESULT = "RESULT_DATA";
    public static final String RESULT_RECIVER = "RESULT_RECEIVER";
    public static final int PARCELABLE_RESULT_CODE = 1;
    public static final int PARCELABLE_ARRAY_RESULT_CODE = 2;
    private ResultReceiverCallBack<T> receiverCallback;

    public CurrencyRatesResultReceiver(Handler handler) {
        super(handler);
    }

    @Override
    protected void onReceiveResult(int resultCode, Bundle bundle) {
        if (receiverCallback != null) {
            if (resultCode == PARCELABLE_RESULT_CODE) {
                receiverCallback.onDataReceive((T) bundle.getParcelable(PARAM_RESULT));
            } else if (resultCode == PARCELABLE_ARRAY_RESULT_CODE) {
                receiverCallback.onDataReceive((T) bundle.getParcelableArrayList(PARAM_RESULT));
            }
        }
    }

    public void setReceiver(ResultReceiverCallBack<T> receiver) {
        this.receiverCallback = receiver;
    }

    public interface ResultReceiverCallBack<T> {
        void onDataReceive(T data);
    }
}

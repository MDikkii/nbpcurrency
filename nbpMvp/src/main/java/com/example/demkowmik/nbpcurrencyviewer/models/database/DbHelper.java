package com.example.demkowmik.nbpcurrencyviewer.models.database;

import com.example.demkowmik.nbpcurrencyviewer.models.CurrencyRate;
import com.example.demkowmik.nbpcurrencyviewer.models.DayTable;

import org.joda.time.LocalDate;

import java.util.Date;
import java.util.Map;

/**
 * Created by Mikolaj D. on 18.10.2017.
 */

public interface DbHelper {
    boolean createDay(DayTable dayTable);

    DayTable getLastAvailableDay();

    DayTable getDay(Date date);

    Map<LocalDate, Double> getCurrencyHistory(String code);

    boolean isEmpty();
}

package com.example.demkowmik.nbpcurrencyviewer.models.interactors;

import android.content.Context;

import com.example.demkowmik.nbpcurrencyviewer.models.DayTable;
import com.example.demkowmik.nbpcurrencyviewer.models.database.DbHelper;
import com.example.demkowmik.nbpcurrencyviewer.models.database.NbpDbFlowHelper;

import org.joda.time.LocalDate;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by demkow.mik on 04.10.2017.
 */

public class CurrencyRatesInteractorImpl implements CurrencyRatesInteractor {
    private static final int DAYS_TO_DOWNLOAD = 90;
    private static final String YEAR_MONTH_DAY_FORMAT = "yyyy-MM-dd";
    private DbHelper db;
    private NbpApi nbpApi;

    public CurrencyRatesInteractorImpl(Context context) {
        db = new NbpDbFlowHelper();
        nbpApi = new NbpApiModuleImpl().createNbpApi();
    }

    @Override
    public List<DayTable> getHistoricTables() {
        Call<List<DayTable>> tablesAPI = nbpApi.getCurrencyTables(
                LocalDate.now().minusDays(DAYS_TO_DOWNLOAD).toString(YEAR_MONTH_DAY_FORMAT),
                LocalDate.now().toString(YEAR_MONTH_DAY_FORMAT));
        List<DayTable> result = new ArrayList<>();
        try {
            Response<List<DayTable>> table = tablesAPI.execute();
            result = table.body();
            if (table.isSuccessful() && result != null) {

                for (DayTable t : result) {
                    db.createDay(t);
                }

            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public DayTable getTodayTable() {
        DayTable todayTable;

        LocalDate date = LocalDate.now();//.plusDays(1);
        todayTable = db.getDay(date.toDate());

        if (todayTable != null) {
            return todayTable;
        }

        //FIXME: endpoint doesn't work for params today, today
        //It works, there is no new table today so it's returns Not found status -
        //to discuss how to improve UX

        //TODO: try to use retrofit & GSON for REST api/data to json conversion
        //Done, doing operation with execute in IntentService, I think it's cleaner way than using
        //so many callbacks as I showed before.
        // It simplifies db operation too, due to IntentService queuing mechanism
        String today = LocalDate.now().toString(YEAR_MONTH_DAY_FORMAT); //.plusDays(1).toString("yyyy-MM-dd");
        Call<List<DayTable>> tablesAPI = nbpApi.getCurrencyTables(today, today);

        try {
            Response<List<DayTable>> result = tablesAPI.execute();
            List<DayTable> tables = result.body();
            if (result.isSuccessful() && tables != null && tables.size() > 0) {
                todayTable = tables.get(0);
                db.createDay(todayTable);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return todayTable;
    }

    @Override
    public DayTable getLastAvailableTable() {
        return db.getLastAvailableDay();
    }

    @Override
    public Map<LocalDate, Double> getHistoricCurrencyTable(String code) {
        return db.getCurrencyHistory(code);
    }

    @Override
    public boolean isDatabaseEmpty() {
        return db.isEmpty();
    }

    @Override
    public long getLastAvailableDate() {
        DayTable lastAvailableTable = db.getLastAvailableDay();

        return lastAvailableTable != null ? lastAvailableTable.getEffectiveDate().getTime() : 0;
    }
}
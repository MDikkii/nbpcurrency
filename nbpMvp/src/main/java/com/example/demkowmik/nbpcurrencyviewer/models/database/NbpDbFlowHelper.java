package com.example.demkowmik.nbpcurrencyviewer.models.database;

import android.util.Log;

import com.example.demkowmik.nbpcurrencyviewer.models.CurrencyRate;
import com.example.demkowmik.nbpcurrencyviewer.models.DayTable;
import com.example.demkowmik.nbpcurrencyviewer.models.DayTable_Table;
import com.example.demkowmik.nbpcurrencyviewer.models.SingleRate_Table;
import com.example.demkowmik.nbpcurrencyviewer.models.database.helperObjects.HistoryRateQueryModel;
import com.example.demkowmik.nbpcurrencyviewer.models.SingleRate;
import com.raizlabs.android.dbflow.sql.language.SQLite;

import org.joda.time.LocalDate;

import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by demkow.mik on 03.10.2017.
 */

public class NbpDbFlowHelper implements DbHelper {
    @Override
    public boolean createDay(DayTable dayTable) {
        dayTable.save();

        addTodayValuesToCurrencyPerDay(dayTable);

        return true;
    }

    @Override
    public DayTable getLastAvailableDay() {
        DayTable day = SQLite.select().from(DayTable.class).orderBy(DayTable_Table.effectiveDate, false).limit(1).querySingle();

        return day;
    }

    @Override
    public DayTable getDay(Date date) {
        DayTable day = SQLite.select().from(DayTable.class).where(DayTable_Table.effectiveDate.eq(date)).querySingle();
        return day;
    }

    private boolean createCurrency(CurrencyRate rate) {
        rate.save();

        return true;
    }

    private void addTodayValuesToCurrencyPerDay(DayTable dayTable) {
        long currencyCounter = SQLite.selectCountOf().from(CurrencyRate.class).count();

        List<CurrencyRate> rates = dayTable.getRates();
        if (currencyCounter == 0) {
            for (CurrencyRate rate : rates) {
                createCurrency(rate);
            }
        }

        for (CurrencyRate rate : rates) {
            createCurrencyValuePerDay(dayTable, rate);
        }
    }

    private void createCurrencyValuePerDay(DayTable dayTable, CurrencyRate rate) {
        SingleRate singleRate = new SingleRate();
        singleRate.value = rate.getValue();
        singleRate.currency = rate;
        singleRate.dayTable = dayTable;

        singleRate.save();
    }

    @Override
    public Map<LocalDate, Double> getCurrencyHistory(String code) {
        Map<LocalDate, Double> result = new TreeMap<>(new Comparator<LocalDate>() {
            @Override
            public int compare(LocalDate localDate, LocalDate t1) {
                return t1.compareTo(localDate);
            }
        });

        Log.e("Query", SQLite.select(DayTable_Table.effectiveDate.as("date"), SingleRate_Table.value)
                .from(DayTable.class)
                .innerJoin(SingleRate.class)
                .on(SingleRate_Table.dayTable_id.eq(DayTable_Table.id))
                .where(SingleRate_Table.currency_code.eq(code))
                .getQuery());

        List<HistoryRateQueryModel> queryResult = SQLite.select(DayTable_Table.effectiveDate.as("date"), SingleRate_Table.value)
                .from(DayTable.class)
                .innerJoin(SingleRate.class)
                .on(SingleRate_Table.dayTable_id.eq(DayTable_Table.id))
                .where(SingleRate_Table.currency_code.eq(code))
                .queryCustomList(HistoryRateQueryModel.class);

        for (HistoryRateQueryModel hrqm : queryResult) {
            result.put(new LocalDate(hrqm.getDate()), hrqm.getValue());
        }

        return result;
    }

    @Override
    public boolean isEmpty() {
        long dayCounter = SQLite.selectCountOf().from(DayTable.class).count();

        return dayCounter == 0;
    }
}
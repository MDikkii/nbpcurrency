package com.example.demkowmik.nbpcurrencyviewer.views;

import com.example.demkowmik.nbpcurrencyviewer.models.DayTable;

import org.joda.time.LocalDate;

/**
 * Created by demkow.mik on 06.10.2017.
 */

public interface MainView extends View {
    void showProgress();

    void hideProgress();

    void setOneDayTableSource(DayTable dayTable);

    void setDate(LocalDate date);

    void startCurrencyRatesIntentService(String action);

    void showOldDataStatus();

    void hideOldDataStatus();

    void showOldDataToast();

    void setUpAlarmForSync();
}
package com.example.demkowmik.nbpcurrencyviewer.views.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.demkowmik.nbpcurrencyviewer.R;

import org.joda.time.LocalDate;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by demkow.mik on 04.10.2017.
 */

public class HistoricCurrencyAdapter extends RecyclerView.Adapter<CurrencyRowViewHolder> {
    List<Map.Entry<LocalDate, Double>> data;

    public HistoricCurrencyAdapter(Context context, Map<LocalDate, Double> dataMap) {
        this.data = new ArrayList<>(dataMap.entrySet());
    }

    @Override
    public CurrencyRowViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.currency_row, parent, false);
        CurrencyRowViewHolder holder = new CurrencyRowViewHolder(v, new CurrencyRowViewHolder.CurrencyRowOnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        return holder;
    }

    @Override
    public void onBindViewHolder(CurrencyRowViewHolder holder, int position) {
        Map.Entry entry = (Map.Entry) data.get(position);

        holder.currencyTitleTextView.setText(((LocalDate) entry.getKey()).toString("dd-MM-yyyy"));
        holder.currencyValueTextView.setText(String.valueOf(entry.getValue()));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }
}
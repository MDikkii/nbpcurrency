package com.example.demkowmik.nbpcurrencyviewer.models.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.demkowmik.nbpcurrencyviewer.models.CurrencyRate;
import com.example.demkowmik.nbpcurrencyviewer.models.DayTable;

import org.joda.time.LocalDate;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by demkow.mik on 03.10.2017.
 */

public class NbpDbHelper extends SQLiteOpenHelper implements DbHelper {
    // Logcat tag
    private static final String LOG = "DatabaseHelper";
    // Database Version
    private static final int DATABASE_VERSION = 1;
    // Database Name
    private static final String DATABASE_NAME = "currencyDatabase";
    // Table Names
    private static final String TABLE_DAY = "day_table";
    private static final String TABLE_CURRENCY = "currency";
    private static final String TABLE_CURRENCY_VALUE_PER_DAY = "currency_value_per_day";
    // Common column names
    private static final String KEY_ID = "id";
    // CURRENCY_TABLE Table - column names
    private static final String KEY_TABLE_TYPE = "table_type";
    private static final String KEY_DATE = "date";
    // Currency Table - column names
    private static final String KEY_CURRENCY_NAME = "currency_name";
    private static final String KEY_CURRENCY_CODE = "currency_code";
    // CURRENCY_VALUE_PER_DAY Table - column names
    private static final String KEY_DAY_ID = "day_id";
    private static final String KEY_CURRENCY_ID = "currency_id";
    private static final String KEY_CURRENCY_VALUE = "currency_value";
    // Table Create Statements
    // DAY table create statement
    private static final String CREATE_TABLE_DAY = "CREATE TABLE "
            + TABLE_DAY + "(" + KEY_ID + " INTEGER PRIMARY KEY," + KEY_TABLE_TYPE
            + " TEXT," + KEY_DATE
            + " DATETIME" + ")";
    // CURRENCY table create statement
    private static final String CREATE_TABLE_CURRENCY = "CREATE TABLE " + TABLE_CURRENCY
            + "(" + KEY_ID + " INTEGER PRIMARY KEY," + KEY_CURRENCY_NAME + " TEXT,"
            + KEY_CURRENCY_CODE + " TEXT" + ")";
    // CURRENCY_VALUE_PER_DAY table create statement
    private static final String CREATE_TABLE_CURRENCY_VALUE_PER_DAY = "CREATE TABLE "
            + TABLE_CURRENCY_VALUE_PER_DAY + "(" + KEY_ID + " INTEGER PRIMARY KEY,"
            + KEY_DAY_ID + " INTEGER," + KEY_CURRENCY_ID + " INTEGER,"
            + KEY_CURRENCY_VALUE + " REAL" + ")";

    private static final String DROP_TABLE_IF_EXISTS = "DROP TABLE IF EXISTS ";
    private Map<String, Long> currencyCodeToId;

    public NbpDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        // creating required tables
        db.execSQL(CREATE_TABLE_DAY);
        db.execSQL(CREATE_TABLE_CURRENCY);
        db.execSQL(CREATE_TABLE_CURRENCY_VALUE_PER_DAY);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // on upgrade drop older tables
        db.execSQL(DROP_TABLE_IF_EXISTS + TABLE_DAY);
        db.execSQL(DROP_TABLE_IF_EXISTS + TABLE_CURRENCY);
        db.execSQL(DROP_TABLE_IF_EXISTS + TABLE_CURRENCY_VALUE_PER_DAY);

        // create new tables
        onCreate(db);
    }

    public boolean createDay(DayTable dayTable) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_TABLE_TYPE, dayTable.getTableType());
        values.put(KEY_DATE, dayTable.getEffectiveDate().getTime());

        long dayId = db.insert(TABLE_DAY, null, values);

        addTodayValuesToCurrencyPerDay(dayTable.getRates(), dayId);

        db.close();

        return true;
    }

    public DayTable getLastAvailableDay() {
        DayTable day = null;
        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery = "SELECT  * FROM " + TABLE_DAY + " ORDER BY "
                + KEY_DATE + " DESC LIMIT 1";

        Log.e(LOG, selectQuery);

        Cursor c = db.rawQuery(selectQuery, null);

        if (c != null)
            if (c.moveToFirst()) {
                day = new DayTable();
                day.setEffectiveDate(new LocalDate(c.getLong(c.getColumnIndex(KEY_DATE))).toDate());
                day.setTableType((c.getString(c.getColumnIndex(KEY_TABLE_TYPE))));
                day.setRates(getAllCurrencyValuesPerDayForDate(c.getInt(c.getColumnIndex(KEY_ID))));
            }


        c.close();
        db.close();
        return day;
    }

    public DayTable getDay(Date date) {
        DayTable day = null;
        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery = "SELECT  * FROM " + TABLE_DAY + " WHERE "
                + KEY_DATE + " = ?";

        Cursor c = db.rawQuery(selectQuery, new String[]{String.valueOf(date.getTime())});

        if (c != null)
            if (c.moveToFirst()) {
                day = new DayTable();
                day.setEffectiveDate(new LocalDate(c.getLong(c.getColumnIndex(KEY_DATE))).toDate());
                day.setTableType((c.getString(c.getColumnIndex(KEY_TABLE_TYPE))));
                day.setRates(getAllCurrencyValuesPerDayForDate(c.getInt(c.getColumnIndex(KEY_ID))));
            }


        c.close();
        db.close();
        return day;
    }

    private List<CurrencyRate> getAllCurrencyValuesPerDayForDate(int dayId) {
        List<CurrencyRate> result = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery = "SELECT " + KEY_CURRENCY_CODE + ", " + KEY_CURRENCY_VALUE + " FROM " + TABLE_CURRENCY + ", " + TABLE_CURRENCY_VALUE_PER_DAY + " WHERE "
                + KEY_DAY_ID + " = ? AND " + TABLE_CURRENCY + "." + KEY_ID + " = " + TABLE_CURRENCY_VALUE_PER_DAY + "." + KEY_CURRENCY_ID;

        Log.e(LOG, selectQuery);

        Cursor c = db.rawQuery(selectQuery, new String[]{String.valueOf(dayId)});

        if (c.moveToFirst()) {
            do {

                CurrencyRate rate = new CurrencyRate();
                String code = c.getString(c.getColumnIndex(KEY_CURRENCY_CODE));
                rate.setCode(code);
                rate.setValue(c.getDouble(c.getColumnIndex(KEY_CURRENCY_VALUE)));
                result.add(rate);
            } while (c.moveToNext());

        }

        c.close();
        db.close();

        return result;
    }

    public boolean createCurrency(CurrencyRate rate) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_CURRENCY_NAME, rate.getName());
        values.put(KEY_CURRENCY_CODE, rate.getCode());

        long currencyId = db.insert(TABLE_CURRENCY, null, values);

        db.close();

        return true;
    }

    public void createCurrencyValuePerDay(long dayId, long currencyId, double rate) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_DAY_ID, dayId);
        values.put(KEY_CURRENCY_ID, currencyId);
        values.put(KEY_CURRENCY_VALUE, rate);

        db.insert(TABLE_CURRENCY_VALUE_PER_DAY, null, values);

        db.close();
    }

    private void addTodayValuesToCurrencyPerDay(List<CurrencyRate> rates, long dayId) {
        SQLiteDatabase db = this.getReadableDatabase();
        long currencyCounter = DatabaseUtils.queryNumEntries(db, TABLE_CURRENCY);
        db.close();

        if (currencyCounter == 0) {
            for (CurrencyRate rate : rates) {
                createCurrency(rate);
            }
        }

        if (currencyCodeToId == null) {
            currencyCodeToId = getAllCurrencyCodesAndIds();
        }

        for (CurrencyRate rate : rates) {
            createCurrencyValuePerDay(dayId, currencyCodeToId.get(rate.getCode()), rate.getValue());
        }
    }

    private Map<String, Long> getAllCurrencyCodesAndIds() {
        Map<String, Long> result = new TreeMap<>();

        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery = "SELECT " + KEY_CURRENCY_CODE + ", " + KEY_ID + " FROM " + TABLE_CURRENCY;

        Log.e(LOG, selectQuery);

        Cursor c = db.rawQuery(selectQuery, null);

        if (c.moveToFirst()) {


            do {
                String code = c.getString(c.getColumnIndex(KEY_CURRENCY_CODE));
                long id = c.getLong(c.getColumnIndex(KEY_ID));
                result.put(code, id);
            } while (c.moveToNext());

        }

        c.close();
        db.close();

        return result;
    }

    // Leave it for testing
    public void clearDatabase() {
        SQLiteDatabase db = this.getWritableDatabase();

        String clearDBQuery = "DELETE FROM " + TABLE_CURRENCY;
        db.execSQL(clearDBQuery);

        clearDBQuery = "DELETE FROM " + TABLE_CURRENCY_VALUE_PER_DAY;
        db.execSQL(clearDBQuery);

        clearDBQuery = "DELETE FROM " + TABLE_DAY;
        db.execSQL(clearDBQuery);

        db.close();
    }

    public Map<LocalDate, Double> getCurrencyHistory(String code) {
        Map<LocalDate, Double> result = new TreeMap<>(new Comparator<LocalDate>() {
            @Override
            public int compare(LocalDate localDate, LocalDate t1) {
                return t1.compareTo(localDate);
            }
        });

        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery = "SELECT " + KEY_DATE + ", " + KEY_CURRENCY_VALUE + " FROM " + TABLE_DAY + ", " + TABLE_CURRENCY + ", " + TABLE_CURRENCY_VALUE_PER_DAY + " WHERE "
                + KEY_CURRENCY_CODE + " = ?" + " AND " + TABLE_CURRENCY + "." + KEY_ID + " = " + TABLE_CURRENCY_VALUE_PER_DAY + "." + KEY_CURRENCY_ID + " AND " + TABLE_DAY + "." + KEY_ID + " = " + TABLE_CURRENCY_VALUE_PER_DAY + "." + KEY_DAY_ID;

        Log.e(LOG, selectQuery);

        Cursor c = db.rawQuery(selectQuery, new String[]{code});

        if (c.moveToFirst()) {
            do {
                LocalDate date = new LocalDate(c.getLong(c.getColumnIndex(KEY_DATE)));
                Double value = c.getDouble(c.getColumnIndex(KEY_CURRENCY_VALUE));
                result.put(date, value);
            } while (c.moveToNext());
        }

        c.close();
        db.close();

        return result;
    }

    public boolean isEmpty() {
        SQLiteDatabase db = this.getReadableDatabase();
        long dayCounter = DatabaseUtils.queryNumEntries(db, TABLE_DAY);
        db.close();
        return dayCounter == 0;
    }
}
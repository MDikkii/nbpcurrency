package com.example.demkowmik.nbpcurrencyviewer.presenters;

/**
 * Created by Mikolaj D. on 09.10.2017.
 */

public interface CurrencyHistoryPresenter {
    void loadData(String code);
}

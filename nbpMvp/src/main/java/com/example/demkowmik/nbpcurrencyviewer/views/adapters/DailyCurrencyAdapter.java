package com.example.demkowmik.nbpcurrencyviewer.views.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.demkowmik.nbpcurrencyviewer.models.CurrencyRate;
import com.example.demkowmik.nbpcurrencyviewer.models.DayTable;
import com.example.demkowmik.nbpcurrencyviewer.R;
import com.example.demkowmik.nbpcurrencyviewer.views.CurrencyHistoryActivity;

import java.util.ArrayList;
import java.util.List;

import static com.example.demkowmik.nbpcurrencyviewer.views.MainActivity.CODE;

/**
 * Created by Mikolaj D. on 04.10.2017.
 */

public class DailyCurrencyAdapter extends RecyclerView.Adapter<CurrencyRowViewHolder> {
    List<CurrencyRate> data;

    public DailyCurrencyAdapter(Context context, DayTable data){
        this.data = (data != null && data.getRates() != null) ? data.getRates() : new ArrayList<CurrencyRate>();
    }

    @Override
    public CurrencyRowViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.currency_row, parent, false);
        CurrencyRowViewHolder holder = new CurrencyRowViewHolder(v, new CurrencyRowViewHolder.CurrencyRowOnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), CurrencyHistoryActivity.class);
                TextView tw = view.findViewById(R.id.currencyTitleTextView);
                intent.putExtra(CODE, tw.getText());
                view.getContext().startActivity(intent);
            }
        });
        return holder;
    }

    @Override
    public void onBindViewHolder(CurrencyRowViewHolder holder, int position) {
        CurrencyRate rate = data.get(position);

        holder.currencyTitleTextView.setText(rate.getCode());
        holder.currencyValueTextView.setText(String.valueOf(rate.getValue()));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }
}
package com.example.demkowmik.nbpcurrencyviewer.models.interactors;

/**
 * Created by Mikolaj D. on 10.10.2017.
 */

interface NbpApiModule {
    NbpApi createNbpApi();
}

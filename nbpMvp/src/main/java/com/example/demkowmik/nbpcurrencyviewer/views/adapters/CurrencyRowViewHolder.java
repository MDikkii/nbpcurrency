package com.example.demkowmik.nbpcurrencyviewer.views.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.demkowmik.nbpcurrencyviewer.R;

/**
 * Created by Mikolaj D. on 11.10.2017.
 */
public class CurrencyRowViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    TextView currencyTitleTextView;
    TextView currencyValueTextView;
    View currencyRow;
    CurrencyRowOnClickListener listener;

    public CurrencyRowViewHolder(View itemView, CurrencyRowOnClickListener listener) {
        super(itemView);

        this.listener = listener;

        currencyRow = itemView.findViewById(R.id.currencyRow);
        currencyTitleTextView = itemView.findViewById(R.id.currencyTitleTextView);
        currencyValueTextView = itemView.findViewById(R.id.currencyValueTextView);

        currencyRow.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        listener.onClick(view);
    }

    public interface CurrencyRowOnClickListener {
        void onClick(View view);
    }
}

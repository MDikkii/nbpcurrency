package com.example.demkowmik.nbpcurrencyviewer.views;

import org.joda.time.LocalDate;

import java.util.Map;

/**
 * Created by Mikolaj D. on 06.10.2017.
 */

public interface CurrencyHistoryView {
    void setUpTableSource(Map<LocalDate, Double> historicData);
    void setUpChart(Map<LocalDate, Double> chartData);
}
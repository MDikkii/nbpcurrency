package com.example.demkowmik.nbpcurrencyviewer.presenters;

import com.example.demkowmik.nbpcurrencyviewer.models.DayTable;

/**
 * Created by Mikolaj D. on 09.10.2017.
 */

public interface MainPresenter {
    void loadData();

    void loadTodayData();

    void showLastAvailableTable();

    void todayTableReceived(DayTable data);

    void lastAvailableDayTableReceived(DayTable data);

    void historicTablesReceived();

    void checkTodayDataAvailability();
}

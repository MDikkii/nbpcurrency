package com.example.demkowmik.nbpcurrencyviewer.presenters;

import android.content.Context;

import com.example.demkowmik.nbpcurrencyviewer.models.interactors.CurrencyRatesInteractor;
import com.example.demkowmik.nbpcurrencyviewer.models.interactors.CurrencyRatesInteractorImpl;
import com.example.demkowmik.nbpcurrencyviewer.views.CurrencyHistoryView;

import org.joda.time.LocalDate;

import java.lang.ref.WeakReference;
import java.util.Comparator;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by Mikolaj D. on 06.10.2017.
 */

public class CurrencyHistoryPresenterImpl implements CurrencyHistoryPresenter {

    private WeakReference<CurrencyHistoryView> currencyHistoryView;
    private CurrencyRatesInteractor currencyRatesInteractor;

    private CurrencyHistoryView getView() throws NullPointerException{
        if ( currencyHistoryView != null )
            return currencyHistoryView.get();
        else
            throw new NullPointerException("View is unavailable");
    }

    public CurrencyHistoryPresenterImpl(CurrencyHistoryView view, Context context){
        currencyHistoryView = new WeakReference<>(view);
        currencyRatesInteractor = new CurrencyRatesInteractorImpl(context);
    }

    @Override
    public void loadData(String code) {
        Map<LocalDate, Double> historicData = currencyRatesInteractor.getHistoricCurrencyTable(code);
        getView().setUpTableSource(historicData);

        Map<LocalDate, Double> chartData = getChartData(historicData);
        getView().setUpChart(chartData);
    }

    private Map<LocalDate,Double> getChartData(Map<LocalDate, Double> historicData) {
        Map<LocalDate, Double> chartData = new TreeMap<>(new Comparator<LocalDate>() {
            @Override
            public int compare(LocalDate localDate, LocalDate t1) {
                return localDate.compareTo(t1);
            }
        });

        chartData.putAll(historicData);

        return chartData;
    }
}
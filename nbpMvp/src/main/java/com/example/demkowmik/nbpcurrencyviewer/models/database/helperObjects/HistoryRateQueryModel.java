package com.example.demkowmik.nbpcurrencyviewer.models.database.helperObjects;

import com.example.demkowmik.nbpcurrencyviewer.models.database.AppDatabase;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.QueryModel;
import com.raizlabs.android.dbflow.structure.BaseQueryModel;

import java.util.Date;

/**
 * Created by Mikolaj D. on 18.10.2017.
 */

@QueryModel(database = AppDatabase.class)
public class HistoryRateQueryModel extends BaseQueryModel {
    @Column
    Date date;
    @Column
    double value;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }
}

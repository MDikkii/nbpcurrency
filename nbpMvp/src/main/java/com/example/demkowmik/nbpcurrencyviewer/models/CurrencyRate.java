package com.example.demkowmik.nbpcurrencyviewer.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.example.demkowmik.nbpcurrencyviewer.models.database.AppDatabase;
import com.google.gson.annotations.SerializedName;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

import java.util.Date;

/**
 * Created by demkow.mik on 03.10.2017.
 */

@Table(database = AppDatabase.class)
public class CurrencyRate extends BaseModel implements Parcelable {

    @PrimaryKey
    private String code;

    @Column
    @SerializedName("currency")
    private String name;

    @SerializedName("mid")
    private double value;

    private Date date;

    protected CurrencyRate(Parcel parcel) {
        name = parcel.readString();
        code = parcel.readString();
        value = parcel.readDouble();
        date = new Date(parcel.readLong());
    }

    public static final Creator<CurrencyRate> CREATOR = new Creator<CurrencyRate>() {
        @Override
        public CurrencyRate createFromParcel(Parcel in) {
            return new CurrencyRate(in);
        }

        @Override
        public CurrencyRate[] newArray(int size) {
            return new CurrencyRate[size];
        }
    };

    public CurrencyRate() {
    }

    public CurrencyRate(String code, String name, double value, Date effectiveDate) {
        this.name = name;
        this.code = code;
        this.value = value;
        this.date = effectiveDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(name);
        parcel.writeString(code);
        parcel.writeDouble(value);
        parcel.writeLong(date.getTime());
    }
}
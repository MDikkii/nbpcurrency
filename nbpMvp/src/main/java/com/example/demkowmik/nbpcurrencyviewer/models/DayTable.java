package com.example.demkowmik.nbpcurrencyviewer.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.example.demkowmik.nbpcurrencyviewer.models.database.AppDatabase;
import com.example.demkowmik.nbpcurrencyviewer.models.database.helperObjects.RateQueryModel;
import com.google.gson.annotations.SerializedName;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.OneToMany;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.raizlabs.android.dbflow.structure.BaseModel;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by demkow.mik on 03.10.2017.
 */


@Table(database = AppDatabase.class)
public class DayTable extends BaseModel implements Parcelable {

    @PrimaryKey(autoincrement = true)
    long id;

    @Column
    @SerializedName("table")
    private String tableType;

    @Column
    private Date effectiveDate;

    private List<CurrencyRate> rates;

    public DayTable() {
        rates = new ArrayList<>();
    }

    protected DayTable(Parcel parcel) {
        tableType = parcel.readString();
        effectiveDate = new Date(parcel.readLong());
        rates = parcel.createTypedArrayList(CurrencyRate.CREATOR);
    }

    public static final Creator<DayTable> CREATOR = new Creator<DayTable>() {
        @Override
        public DayTable createFromParcel(Parcel in) {
            return new DayTable(in);
        }

        @Override
        public DayTable[] newArray(int size) {
            return new DayTable[size];
        }
    };

    public String getTableType() {
        return tableType;
    }

    public void setTableType(String tableType) {
        this.tableType = tableType;
    }

    public Date getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(Date effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public List<CurrencyRate> getRates() {
        return rates;
    }

    public void setRates(List<CurrencyRate> rates) {
        this.rates = rates;
        for (CurrencyRate r : rates) {
            r.setDate(getEffectiveDate());
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(tableType);
        parcel.writeLong(effectiveDate.getTime());
        parcel.writeTypedList(rates);
    }

    @OneToMany(methods = {OneToMany.Method.ALL}, variableName = "rates")
    public List<CurrencyRate> getDayRates() {
        if (rates == null || rates.isEmpty()) {
            List<RateQueryModel> queryModels = SQLite.select(CurrencyRate_Table.code, CurrencyRate_Table.name, SingleRate_Table.value)
                    .from(CurrencyRate.class)
                    .innerJoin(SingleRate.class)
                    .on(SingleRate_Table.dayTable_id.eq(id))
                    .where(SingleRate_Table.currency_code.eq(CurrencyRate_Table.code))
                    .queryCustomList(RateQueryModel.class);

            rates = new ArrayList<>();

            for(RateQueryModel qm : queryModels)
            {
                rates.add(new CurrencyRate(qm.getCode(), qm.getName(), qm.getValue(), effectiveDate));
            }
        }

        return rates;
    }
}
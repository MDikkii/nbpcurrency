package com.example.linechartmodule;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

import java.util.List;

/**
 * Created by Mikolaj D. on 23.10.2017.
 */

public class LineChart extends View {
    public static final int SPACE_FOR_AXIS = 100;
    public static final int AXIS_ARROW_LENGTH = 20;
    public static final int AXIS_ARROW_SIZE = 30;
    public static final int AXIS_DASH_NUMBER = 15;
    public static final int AXIS_DASH_LENGTH = 10;
    public static final int POINT_RADIUS = 10;
    ChartData chartData;
    private boolean showTitleLabel;
    private Paint titlePaint;
    private Paint gridPaint;
    private float gridXEnd;
    private float gridYEnd;
    private float gridXStart;
    private float gridYStart;
    private Paint pointPaint;
    private Paint linePaint;
    private float minYValue;
    private float maxYValue;
    private float yFactor;
    private float maxXValue;
    private float minXValue;
    private float xFactor;
    private Paint axisLabelPaint;
    private GestureDetector detector;

    public LineChart(Context context) {
        super(context);
        init();
    }

    public LineChart(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.LineChart,
                0, 0);

        try {
            showTitleLabel = a.getBoolean(R.styleable.LineChart_showTitleLabel, false);
        } finally {
            a.recycle();
        }

        init();
    }

    private void init() {
        titlePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        titlePaint.setColor(Color.GREEN);
        titlePaint.setTextSize(40f);

        gridPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        gridPaint.setColor(Color.RED);
        gridPaint.setStrokeWidth(5f);

        pointPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        pointPaint.setColor(Color.BLUE);
        pointPaint.setStrokeWidth(40f);

        linePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        linePaint.setColor(Color.BLUE);
        linePaint.setStrokeWidth(5f);

        axisLabelPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        axisLabelPaint.setTextSize(40f);

        detector = new GestureDetector(getContext(), new GestureListener());
    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if (showTitleLabel) {
            canvas.drawText(chartData.title, gridXStart, gridYEnd + titlePaint.getTextSize() * 3, titlePaint);
        }


        setFactors();
        drawGrid(canvas);
        drawSeries(canvas);
        drawAxisLabels(canvas);
    }

    private void drawAxisLabels(Canvas canvas) {
        canvas.drawText(String.valueOf(minXValue), gridXStart, gridYEnd + axisLabelPaint.getTextSize() * 2, axisLabelPaint);
        canvas.drawText(String.valueOf(maxXValue), gridXEnd, gridYEnd + axisLabelPaint.getTextSize() * 2, axisLabelPaint);

        canvas.drawText(String.valueOf(minYValue), gridXStart - SPACE_FOR_AXIS, gridYEnd, axisLabelPaint);
        canvas.drawText(String.valueOf(maxYValue), gridXStart - SPACE_FOR_AXIS, gridYStart, axisLabelPaint);
    }

    private void setFactors() {
        List<LineDataSerie> series = chartData.dataSeries;

        float gridHeight = gridYEnd - gridYStart;
        float gridWidth = gridXEnd - gridXStart;

        minYValue = Float.MAX_VALUE;
        maxYValue = Float.MIN_VALUE;
        minXValue = Float.MAX_VALUE;
        maxXValue = Float.MIN_VALUE;


        for (LineDataSerie s : series) {
            for (ChartPoint cp : s.points) {

                if (cp.yValue < minYValue) {
                    minYValue = cp.yValue;
                }

                if (cp.yValue > maxYValue) {
                    maxYValue = cp.yValue;
                }

                if (cp.xValue < minXValue) {
                    minXValue = cp.xValue;
                }

                if (cp.xValue > maxXValue) {
                    maxXValue = cp.xValue;
                }
            }
        }

        float yValuesDifference = maxYValue - minYValue;
        yFactor = gridHeight / yValuesDifference;

        float xValuesDifference = maxXValue - minXValue;
        xFactor = gridWidth / xValuesDifference;
    }

    private void drawGrid(Canvas canvas) {
        float xAxisYPosition = gridYEnd;
        //TODO: Y axis for <0 values
//        if(minYValue < 0){
//            xAxisYPosition = gridYEnd - gridYEnd / yFactor;
//        }
        canvas.drawLine(gridXStart, xAxisYPosition, gridXEnd, xAxisYPosition, gridPaint);
        canvas.drawLine(gridXStart, gridYEnd, gridXStart, gridYStart, gridPaint);

        for (int i = 2; i <= AXIS_DASH_NUMBER; i++) {
            canvas.drawLine(i * (gridXEnd - gridXStart) / AXIS_DASH_NUMBER, gridYEnd + AXIS_DASH_LENGTH, i * (gridXEnd - gridXStart) / AXIS_DASH_NUMBER, gridYEnd - AXIS_DASH_LENGTH, gridPaint);
            canvas.drawLine(gridXStart - AXIS_DASH_LENGTH, i * gridYEnd / AXIS_DASH_NUMBER, gridXStart + AXIS_DASH_LENGTH, i * gridYEnd / AXIS_DASH_NUMBER, gridPaint);
        }

        canvas.drawLine(gridXEnd - AXIS_ARROW_LENGTH, gridYEnd + AXIS_ARROW_SIZE, gridXEnd, gridYEnd, gridPaint);
        canvas.drawLine(gridXEnd - AXIS_ARROW_LENGTH, gridYEnd - AXIS_ARROW_SIZE, gridXEnd, gridYEnd, gridPaint);

        canvas.drawLine(gridXStart - AXIS_ARROW_SIZE, gridYStart + AXIS_ARROW_LENGTH, gridXStart, gridYStart, gridPaint);
        canvas.drawLine(gridXStart + AXIS_ARROW_SIZE, gridYStart + AXIS_ARROW_LENGTH, gridXStart, gridYStart, gridPaint);
    }

    private void drawSeries(Canvas canvas) {
        List<LineDataSerie> series = chartData.dataSeries;


        for (int i = 0; i < series.size(); i++) {
            ChartPoint firstPoint = series.get(i).points.get(0);
            float yOldConvertedValue = gridYEnd - (firstPoint.yValue - minYValue) * yFactor;
            float xOldConvertedValue = gridXStart + (firstPoint.xValue - minXValue) * xFactor;
            //TODO: different color for different series
            //    pointPaint.setColor(Color.BLUE + i);
            //    linePaint.setColor(Color.BLUE + i);

            for (ChartPoint cp : series.get(i).points) {

                float yConvertedValue = gridYEnd - (cp.yValue - minYValue) * yFactor;
                float xConvertedValue = gridXStart + (cp.xValue - minXValue) * xFactor;

                canvas.drawOval(xConvertedValue - POINT_RADIUS, yConvertedValue + POINT_RADIUS, xConvertedValue + POINT_RADIUS, yConvertedValue - POINT_RADIUS, pointPaint);
                canvas.drawLine(xOldConvertedValue, yOldConvertedValue, xConvertedValue, yConvertedValue, linePaint);

                yOldConvertedValue = yConvertedValue;
                xOldConvertedValue = xConvertedValue;
            }
        }
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        float xLeftSpace = Math.max(getPaddingLeft(), getPaddingStart()) + SPACE_FOR_AXIS;
        float xRightSpace = Math.max(getPaddingRight(), getPaddingEnd()) + SPACE_FOR_AXIS;
        float yTopSpace = getPaddingTop() + SPACE_FOR_AXIS;
        float yBottomSpace = getPaddingBottom() + SPACE_FOR_AXIS;

        if (showTitleLabel) {
            yBottomSpace += titlePaint.getTextSize() * 3;
        }

        gridXStart = xLeftSpace;
        gridYStart = yTopSpace;
        gridXEnd = w - xRightSpace;
        gridYEnd = h - yBottomSpace;

    }

    private void setLabel(MotionEvent e) {
        Log.e("Label xy", e.getX() + " " + e.getY());
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        // Let the GestureDetector interpret this event
        boolean result = detector.onTouchEvent(event);

        // If the GestureDetector doesn't want this event, do some custom processing.
        // This code just tries to detect when the user is done scrolling by looking
        // for ACTION_UP events.
        if (!result) {
            if (event.getAction() == MotionEvent.ACTION_UP) {
                // User is done scrolling, it's now safe to do things like autocenter
              //  stopScrolling();
                result = true;
            }
        }
        return result;
    }

    private class GestureListener extends GestureDetector.SimpleOnGestureListener {


        @Override
        public boolean onSingleTapUp(MotionEvent e) {
            setLabel(e);
            return true;
        }

        @Override
        public boolean onDown(MotionEvent e) {
            setLabel(e);
            return true;
        }

        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) {
            setLabel(e);
            return true;
        }
    }
}

package com.example.linechartmodule;

import java.util.List;

/**
 * Created by Mikolaj D. on 23.10.2017.
 */

class LineDataSerie {
    String serieTitle;
    List<ChartPoint> points;
}

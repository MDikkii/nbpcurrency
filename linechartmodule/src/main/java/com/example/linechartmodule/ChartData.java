package com.example.linechartmodule;

import java.util.List;

/**
 * Created by Mikolaj D. on 23.10.2017.
 */

public class ChartData {
    String title;
    List<LineDataSerie> dataSeries;
}

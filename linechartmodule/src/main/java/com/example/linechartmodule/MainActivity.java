package com.example.linechartmodule;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        LineChart lineChart = findViewById(R.id.line_chart);
        lineChart.chartData = new ChartData();
        lineChart.chartData.title = "Title";

        LineDataSerie lineDataSerie = new LineDataSerie();
        lineDataSerie.points = new ArrayList<>();
        lineDataSerie.points.add(new ChartPoint(100f, 200f));
        lineDataSerie.points.add(new ChartPoint(200f, 300f));
        lineDataSerie.points.add(new ChartPoint(300f, 250f));
        lineDataSerie.points.add(new ChartPoint(400f, 340f));
        lineDataSerie.points.add(new ChartPoint(500f, 43f));

        LineDataSerie lineDataSerie1 = new LineDataSerie();
        lineDataSerie1.points = new ArrayList<>();
        lineDataSerie1.points.add(new ChartPoint(150f, 500f));
        lineDataSerie1.points.add(new ChartPoint(250f, 523f));
        lineDataSerie1.points.add(new ChartPoint(350f, 520f));
        lineDataSerie1.points.add(new ChartPoint(450f, 544f));
        lineDataSerie1.points.add(new ChartPoint(480f, 498f));

        lineChart.chartData.dataSeries = new ArrayList<>();
        lineChart.chartData.dataSeries.add(lineDataSerie);
        lineChart.chartData.dataSeries.add(lineDataSerie1);

    }
}

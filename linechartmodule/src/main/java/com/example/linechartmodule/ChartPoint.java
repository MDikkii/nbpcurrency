package com.example.linechartmodule;

/**
 * Created by Mikolaj D. on 23.10.2017.
 */

class ChartPoint {
    float xValue;
    float yValue;

    public ChartPoint(float xVal, float yVal) {
        xValue = xVal;
        yValue = yVal;
    }
}
